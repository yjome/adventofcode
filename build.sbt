name := "aoc2020-scala"

version := "0.1"

scalaVersion := "3.1.0"

libraryDependencies ++= Seq(
  "com.lihaoyi" %% "utest" % "0.7.10" % "test",
  "org.scala-lang.modules" %% "scala-parser-combinators" % "2.1.0",
  "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4",
  "org.scala-lang.modules" % "scala-collection-contrib_2.13" % "0.2.2",
  "org.scodec" %% "scodec-core" % "2.1.0",

)

testFrameworks += new TestFramework("utest.runner.Framework")
