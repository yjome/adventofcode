package me.yjo.aoc.y2020

import me.yjo.aoc.y2020.Day24.HexCoord.dirs
import me.yjo.util._
import utest._

import scala.util.parsing.combinator.RegexParsers

object Day24 extends DayBase {

  case class HexCoord(x: Int, y: Int) {
    def +(other: HexCoord): HexCoord = HexCoord(x + other.x, y + other.y)

    def neighbours: Iterable[HexCoord] = dirs.values.map(this + _)
  }

  object HexCoord {
    val dirs: Map[String, HexCoord] = Map(
      "ne" -> HexCoord(1, -1),
      "e" -> HexCoord(1, 0),
      "se" -> HexCoord(0, 1),
      "sw" -> HexCoord(-1, +1),
      "w" -> HexCoord(-1, 0),
      "nw" -> HexCoord(0, -1)
    )
  }

  object HexCoordParser extends RegexParsers {
    val dir = HexCoord.dirs.map { case (name, coord) => name ^^^ coord }.reduce(_ | _)
    val dirs = rep1(dir)

    def parseDirs(in: String): List[HexCoord] = parseAll(dirs, in).get
  }

  def blackTilesFromInput(input: String): Set[HexCoord] = {
    val termini = input.linesIterator.map(HexCoordParser.parseDirs(_).reduce(_ + _))
    counts(termini).filter(_._2 % 2 == 1).keys.toSet
  }

  def s1(input: String): Long =
    blackTilesFromInput(input).size

  case class State(black: Set[HexCoord]) {
    def next: State = {
      val neighbourCounts = counts(black.iterator.flatMap(_.neighbours))
      State(neighbourCounts.filter { case (coord, count) =>
        count == 2 || count == 1 && black(coord)
      }.keys.toSet)
    }
  }

  def s2(input: String): Long = {
    val start = State(blackTilesFromInput(input))
    nTimes[State](100, _.next, start).black.size
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 10
      theAnswerIs(s1(input), 485)
    }
    test("star2") {
      s2(testInput) ==> 2208
      theAnswerIs(s2(input), 3933)
    }
  }
}
