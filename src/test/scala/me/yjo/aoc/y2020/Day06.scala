package me.yjo.aoc.y2020

import utest._

object Day06 extends DayBase {

  override lazy val testInput = "abc\n\na\nb\nc\n\nab\nac\n\na\na\na\na\n\nb"

  def s1(input: String) =
    input.split("\n\n").map(s =>
      s.filter('a' to 'z' contains _)
      .toSet
    ).map(_.size).sum

  def s2(input: String) =
    input.split("\n\n").map(s =>
      s.linesIterator.map(_.toSet).reduce(_ intersect _)
    ).map(_.size).sum

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 11
      theAnswerIs(s1(input), 6521)
    }
    test("star2") {
      s2(testInput) ==> 6
      theAnswerIs(s2(input), 3305)
    }
  }
}
