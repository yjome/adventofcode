package me.yjo.aoc.y2020

import me.yjo.util._
import utest._

import java.lang.Math.floorMod

object Day25 extends DayBase {

  def solve(pub1: Int, pub2: Int): Long = {
    val priv1 = Iterator.iterate((0, 1L)) {
      case (i, acc) => (i + 1, floorMod(acc * 7, 20201227))
    }.find(_._2 == pub1).get._1

    nTimes[Long](priv1, n => floorMod(n * pub2, 20201227), 1)
  }

  val tests = Tests {
    test("star1") {
      solve(17807724, 5764801) ==> 14897079
      theAnswerIs(solve(1614360, 7734663), 5414549)
    }
  }
}
