package me.yjo.aoc.y2020

import me.yjo.aoc.y2020.Day08.Machine.{Instr, Word}
import me.yjo.util.IntString
import utest._

import scala.collection.mutable

object Day08 extends DayBase {

  case class Machine(prog: Vector[Instr], ip: Int = 0, acc: Word = 0) {
    require(prog.indices.contains(ip) || isFinished)

    def isFinished = ip == prog.size

    def next: Machine = prog(ip) match {
      case Instr("acc", arg) => copy(ip = ip + 1, acc = acc + arg)
      case Instr("jmp", arg) => copy(ip = (ip + arg).toInt)
      case Instr("nop", _) => copy(ip = ip + 1)
    }

    def runUntilFinishOrLoop(): Machine = {
      var machine = this
      val visitedIPs = mutable.Set(machine.ip)
      while ( {
        machine = machine.next
        visitedIPs.add(machine.ip) && !machine.isFinished
      }) ()
      machine
    }
  }

  object Machine {

    type Word = Long

    def apply(program: String): Machine = {
      Machine(program.linesIterator.map(Instr.apply).toVector)
    }

    case class Instr(op: String, arg: Word)

    object Instr {
      def apply(line: String): Instr = line match {
        case s"${op} ${IntString(arg)}" => Instr(op, arg)
      }
    }

  }

  def s1(input: String) = {
    Machine(input).runUntilFinishOrLoop().acc
  }

  def s2(input: String) = {
    def toggle(op: String) = op match {
      case "nop" => Some("jmp")
      case "jmp" => Some("nop")
      case _ => None
    }

    val baseProg = Machine(input).prog
    (for {
      ix <- baseProg.indices.view
      Instr(op, arg) = baseProg(ix)
      newOp <- toggle(op)
      modifiedProg = baseProg.updated(ix, Instr(newOp, arg))
      result = Machine(modifiedProg).runUntilFinishOrLoop()
      if result.isFinished
    } yield result).head.acc
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 5
      theAnswerIs(s1(input), 1818)
    }
    test("star2") {
      s2(testInput) ==> 8
      theAnswerIs(s2(input), 631)
    }
  }
}
