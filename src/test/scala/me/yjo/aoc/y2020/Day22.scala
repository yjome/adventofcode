package me.yjo.aoc.y2020

import me.yjo.util._
import utest._

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object Day22 extends DayBase {
  type Deck = Queue[Int]

  def parse(in: String): (Deck, Deck) = {
    val Array(d1, d2) = in.split("\n\n")
      .map(s => Queue.from(readInts(s.dropWhile(_ != '\n'))))
    (d1, d2)
  }

  def score(d: Deck): Long =
    d.reverse.zipWithIndex.map { case (n, i) => n.toLong * (i + 1) }.sum

  @tailrec
  def playS1(d1: Deck, d2: Deck): Long = {
    if (d1.isEmpty) score(d2)
    else if (d2.isEmpty) score(d1)
    else if (d1.head > d2.head)
      playS1(d1.tail.enqueueAll(Seq(d1.head, d2.head)), d2.tail)
    else
      playS1(d1.tail, d2.tail.enqueueAll(Seq(d2.head, d1.head)))
  }

  def s1(input: String): Long = {
    val (d1, d2) = parse(input)
    playS1(d1, d2)
  }

  def d1WinsRound(d1: Deck, d2: Deck): Boolean = {
    if (d1.sizeIs >= d1.head + 1 && d2.sizeIs >= d2.head + 1)
      playS2(d1.tail.take(d1.head), d2.tail.take(d2.head), Set()).isLeft
    else d1.head > d2.head
  }

  @tailrec
  def playS2(d1: Deck, d2: Deck, seen: Set[(Deck, Deck)]): Either[Long, Long] = {
    if (seen.contains((d1, d2))) Left(score(d1))
    else if (d1.isEmpty) Right(score(d2))
    else if (d2.isEmpty) Left(score(d1))
    else if (d1WinsRound(d1, d2))
      playS2(d1.tail ++ Seq(d1.head, d2.head), d2.tail, seen + ((d1, d2)))
    else
      playS2(d1.tail, d2.tail ++ Seq(d2.head, d1.head), seen + ((d1, d2)))
  }

  def s2(input: String): Long = {
    val (d1, d2) = parse(input)
    playS2(d1, d2, Set()).merge
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 306
      theAnswerIs(s1(input), 32033)
    }
    test("star2") {
      s2(testInput) ==> 291
      theAnswerIs(s2(input), 34901)
    }
  }
}
