package me.yjo.aoc.y2020

import me.yjo.util.IntString
import utest._

object Day02 extends DayBase {

  override lazy val testInput = "1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc"

  val PwPattern = """(\d+)-(\d+) ([a-z]): ([a-z]+)""".r

  case class PwLine(min: Int, max: Int, c: Char, pw: String) {
    val countC = pw.count(pc => pc == c)
    def isValid = countC >= min && countC <= max
    def isValid2 = pw(min - 1) == c ^ pw(max - 1) == c
  }

  def parse(line: String): PwLine = line match {
    case PwPattern(IntString(min), IntString(max), c, pw) => PwLine(min, max, c(0), pw)
  }

  val tests = Tests {
    test("star1") {
      testInput.lines().map(parse).filter(_.isValid).count() ==> 2
      theAnswerIs(input.lines().map(parse).filter(_.isValid).count(), 660)
    }
    test("star2") {
      testInput.lines().map(parse).filter(_.isValid2).count() ==> 1
      theAnswerIs(input.lines().map(parse).filter(_.isValid2).count(), 530)
    }
  }
}
