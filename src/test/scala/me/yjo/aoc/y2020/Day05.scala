package me.yjo.aoc.y2020

import utest._

object Day05 extends DayBase {

  def parseBin(str: String, one: Char, zero: Char): Int =
    Integer.parseInt(str.replace(one, '1').replace(zero, '0'), 2)

  case class Pass(row: Int, col: Int) {
    val id = row * 8 + col
  }

  object Pass {
    def apply(code: String): Pass = Pass(
      parseBin(code.take(7), 'B', 'F'),
      parseBin(code.drop(7), 'R', 'L')
    )
  }

  lazy val passes = input.linesIterator.map(Pass.apply).toSet

  val tests = Tests {
    test("star1") {
      Pass("BFFFBBFRRR").id ==> 567
      Pass("FFFBBBFRRR").id ==> 119
      Pass("BBFFBBFRLL").id ==> 820
      theAnswerIs(passes.map(_.id).max, 959)
    }
    test("star2") {
      val rows = passes.map(_.row)
      val possibleSeats = (for {
        row <- rows.min + 1 until rows.max
        col <- 0 to 7
      } yield Pass(row, col)).toSet
      theAnswerIs(possibleSeats.diff(passes).map(_.id), Set(527))
    }
  }
}
