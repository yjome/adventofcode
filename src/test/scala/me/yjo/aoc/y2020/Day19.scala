package me.yjo.aoc.y2020

import me.yjo.util._
import utest._

object Day19 extends DayBase {

  case class Rules(byId: Map[Int, String]) {
    val patternById: Int => String = memoise[Int, String] { id =>
      byId(id) match {
        case s""""${literal}"""" => literal
        case rule => "(?:" + rule.split("\\|").map("(?:" + readInts(_).map(patternById).mkString + ")").mkString("|") + ")"
      }
    }

    // Yuck:
    val patternById2: Int => String = memoise {
      case 8 => s"(?:${patternById2(42)}+)"
      case 11 => "(?:" + (for (i <- 1 to 10) yield s"(?:(?:${patternById2(42)}{$i})(?:${patternById2(31)}{$i}))").mkString("|") + ")"
      case id => byId(id) match {
        case s""""${literal}"""" => literal
        case rule => "(?:" + rule.split("\\|").map("(?:" + readInts(_).map(patternById2).mkString + ")").mkString("|") + ")"
      }
    }

    def test(in: String) = in.matches(patternById(0))

    def test2(in: String) = in.matches(patternById2(0))
  }

  object Rules {
    def apply(rulesStr: String): Rules =
      Rules(rulesStr.linesIterator.map {
        case s"${IntString(n)}: ${rule}" => n -> rule
      }.toMap)
  }

  def parse(input: String): (Rules, Seq[String]) = {
    val Array(rulesStr, examplesStr) = input.split("\n\n")
    (Rules(rulesStr), examplesStr.linesIterator.toSeq)
  }

  def s1(input: String): Long = {
    val (rules, examples) = parse(input)
    examples.count(rules.test)
  }

  def s2(input: String): Long = {
    val (rules, examples) = parse(input)
    examples.count(rules.test2)
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 2
      theAnswerIs(s1(input), 239)
    }
    test("star2") {
      s2(readDataFile("Day19.test2.txt"))==> 12
      theAnswerIs(s2(input), 405)
    }
  }
}
