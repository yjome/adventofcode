package me.yjo.aoc.y2020

import me.yjo.util._
import utest._

import java.lang.Math.floorMod
import scala.collection.View

object Day23 extends DayBase {

  override lazy val input = "193467258"
  override lazy val testInput = "389125467"

  case class State(successor: Map[Int, Int], current: Int) {
    def size = successor.size

    def view: View[Int] = viewFrom(current)

    def viewFrom(start: Int): View[Int] =
      View.iterate(start, size)(successor)

    def next: State = {
      val selection@Seq(selHead, _, selLast) = view.slice(1, 4).toSeq
      val destination = Iterator.iterate(current)(index => floorMod(index - 2, size) + 1)
        .drop(1)
        .find(!selection.contains(_))
        .get
      val nextCurrent = successor(selLast)
      val nextSuccessor = successor +
        (current -> nextCurrent) +
        (destination -> selHead) +
        (selLast -> successor(destination))
      State(nextSuccessor, nextCurrent)
    }
  }

  object State {
    def apply(in: Iterable[Int]): State =
      State(in.zip(in.tail ++ Iterable(in.head)).toMap, in.head)
  }

  def s1(input: String): String = {
    nTimes[State](100, _.next, State(input.map(_.asDigit))).viewFrom(1).drop(1).mkString
  }

  def s2(in: String): Long = {
    val start = State(in.map(_.asDigit) ++ (10 to 1_000_000))
    val end = nTimes[State](10_000_000, _.next, start)
    end.viewFrom(1).slice(1, 3).map(_.toLong).product
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> "67384529"
      theAnswerIs(s1(input), "25468379")
    }
    test("star2") {
      s2(testInput) ==> 149245887792L
      theAnswerIs(s2(input), 474747880250L)
    }
  }
}
