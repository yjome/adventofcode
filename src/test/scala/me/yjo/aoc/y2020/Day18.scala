package me.yjo.aoc.y2020

import me.yjo.util._
import utest._

import scala.util.parsing.combinator.RegexParsers

object Day18 extends DayBase {

  object Calculator extends RegexParsers {
    def literal: Parser[Long] = """\d+""".r ^^ { _.toLong }
    def factor: Parser[Long] = literal | "(" ~> expr <~ ")"
    def expr: Parser[Long] = factor ~ rep( "*" ~ factor | "+" ~ factor) ^^ {
      case number ~ list => list.foldLeft(number) {
        case (x, "*" ~ y) => Math.multiplyExact(x, y)
        case (x, "+" ~ y) => Math.addExact(x, y)
      }
    }

    def apply(input: String): Long = parseAll(expr, input) match {
      case Success(result, _) => result
      case failure : NoSuccess => scala.sys.error(failure.msg)
    }
  }

  def s1(input: String): Long = {
    input.linesIterator.map(Calculator.apply).sum
  }

  object Calculator2 extends RegexParsers {
    def literal: Parser[Long] = """\d+""".r ^^ { _.toLong }
    def factor: Parser[Long] = literal | "(" ~> expr <~ ")"
    def term: Parser[Long] = factor ~ rep( "+" ~ factor) ^^ {
      case number ~ list => list.foldLeft(number) {
        case (x, "+" ~ y) => Math.addExact(x, y)
      }
    }
    def expr: Parser[Long] = term ~ rep("*" ~ term) ^^ {
      case number ~ list => list.foldLeft(number) {
        case (x, "*" ~ y) => Math.multiplyExact(x, y)
      }
    }

    def apply(input: String): Long = parseAll(expr, input) match {
      case Success(result, _) => result
      case failure : NoSuccess => scala.sys.error(failure.msg)
    }
  }

  def s2(input: String): Long = {
    input.linesIterator.map(Calculator2.apply).sum
  }

  val tests = Tests {
    test("star1") {
      s1("2 * 3 + (4 * 5)") ==> 26
      s1("5 + (8 * 3 + 9 + 3 * 4 * 3)") ==> 437
      s1("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))") ==> 12240
      s1("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2") ==> 13632
      theAnswerIs(s1(input), 7293529867931L)
    }
    test("star2") {
      s2("2 * 3 + (4 * 5)") ==> 46
      s2("5 + (8 * 3 + 9 + 3 * 4 * 3)") ==> 1445
      s2("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))") ==> 669060
      s2("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2") ==> 23340
      theAnswerIs(s2(input), 60807587180737L)
    }
  }
}
