package me.yjo.aoc.y2020

import me.yjo.maths.ExactMathsImplicits
import utest.*

import scala.io.{Codec, Source}

abstract class DayBase extends utest.TestSuite {
  protected def readDataFile(filename: String): String =
    try {
      Source.fromInputStream(getClass.getResourceAsStream(filename))(Codec.UTF8).mkString
    } catch {
      case _: Exception => throw new RuntimeException(s"""Couldn't open file '$filename"""")
    }

  def objectName = getClass.getSimpleName.replaceAll("\\$$", "")

  lazy val input: String = readDataFile(objectName + ".txt")

  lazy val testInput: String = readDataFile(objectName + ".test.txt")

  @deprecated("Remember to confirm answer")
  def theAnswerIs(output: Any): Unit =
    println(output)

  def theAnswerIs[T](output: T, whichShouldEqual: T): Unit = {
    output ==> whichShouldEqual
    println(output)
  }

  protected implicit val intIsExactIntegral: Integral[Int] = ExactMathsImplicits.IntIsExactIntegral
  protected implicit val longIsExactIntegral: Integral[Long] = ExactMathsImplicits.LongIsExactIntegral
}
