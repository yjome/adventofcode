package me.yjo.aoc.y2020

import me.yjo.util.IntString
import utest._

object Day13 extends DayBase {

  override lazy val testInput = "939\n7,13,x,x,59,x,31,19"

  def s1(input: String): Int = {
    val Seq(IntString(startTime), line2) = input.linesIterator.toSeq
    val buses = line2.split(',').filter(_ != "x").map(_.toInt).toSet
    val bus = buses.minBy(Math.floorMod(-startTime, _))
    bus * Math.floorMod(-startTime, bus)
  }

  // From https://brilliant.org/wiki/chinese-remainder-theorem/
  def solveCRT(remaindersByModulus: Iterable[(BigInt, BigInt)]): BigInt = {
    val N = remaindersByModulus.map(_._1).product
    (for {
      (n, a) <- remaindersByModulus
      y = N / n
      z = y modInverse n
    } yield a * y * z).sum mod N
  }

  def s2(input: String): BigInt = {
    val buses = for {
      (str, offset) <- input.linesIterator.toSeq.last.split(',').zipWithIndex
      bus <- str.toIntOption
    } yield BigInt(bus) -> BigInt(-offset)
    solveCRT(buses)
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 295
      theAnswerIs(s1(input), 2298)
    }
    test("star2") {
      solveCRT(Seq(
        BigInt(3) -> BigInt(1), BigInt(5) -> BigInt(4), BigInt(7) -> BigInt(6))
      ) ==> 34
      s2(testInput) ==> 1068781
      s2("67,7,59,61") ==> 754018
      s2("67,x,7,59,61") ==> 779210
      s2("67,7,x,59,61") ==> 1261476
      s2("1789,37,47,1889") ==> 1202161486
      theAnswerIs(s2(input), 783685719679632L)
    }
  }
}
