package me.yjo.aoc.y2020

import me.yjo.util._
import utest._

object DayXX extends DayBase {

  def s1(input: String): Long = {
    ???
  }

  def s2(input: String): Long = {
    ???
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> ???
      theAnswerIs(s1(input))
    }
    test("star2") {
      s2(testInput) ==> ???
      theAnswerIs(s2(input))
    }
  }
}
