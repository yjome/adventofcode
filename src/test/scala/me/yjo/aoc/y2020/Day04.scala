package me.yjo.aoc.y2020

import me.yjo.util._
import me.yjo.util.IntString
import utest._

import scala.util.matching.Regex.Groups

object Day04 extends DayBase {

  val entryPattern = """([a-z]+):(\S+)""".r

  def parse(input: String) = {
    input.split("\n\n").toSeq.map(in => {
      (for {
        Groups(k, v) <- entryPattern.findAllMatchIn(in)
      } yield k -> v).toMap
    })
  }

  def isValid(passport: Map[String, String]) = {
    Seq("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid").forall(f => passport.contains(f))
  }

  def isValid2(p: Map[String, String]): Boolean = {
    isValid(p) && p.forall {
      case "byr" -> IntString(byr)                 => byr >= 1920 && byr <= 2002
      case "iyr" -> IntString(iyr)                 => iyr >= 2010 && iyr <= 2020
      case "eyr" -> IntString(eyr)                 => eyr >= 2020 && eyr <= 2030
      case "hgt" -> s"${IntString(cms)}cm"         => cms >= 150 && cms <= 193
      case "hgt" -> s"${IntString(ins)}in"         => ins >= 59 && ins <= 76
      case "hcl" -> r"#[0-9a-f]{6}"                => true
      case "ecl" -> r"amb|blu|brn|gry|grn|hzl|oth" => true
      case "pid" -> r"[0-9]{9}"                    => true
      case "cid" -> _                              => true
      case _     -> _                              => false
    }
  }

  val tests = Tests {
    test("star1") {
      parse(testInput).count(isValid) ==> 2
      theAnswerIs(parse(input).count(isValid), 204)
    }
    test("star2") {
      parse("eyr:1972 cid:100\nhcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926\n\niyr:2019\nhcl:#602927 eyr:1967 hgt:170cm\necl:grn pid:012533040 byr:1946\n\nhcl:dab227 iyr:2012\necl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277\n\nhgt:59cm ecl:zzz\neyr:2038 hcl:74454a iyr:2023\npid:3556412378 byr:2007")
        .count(isValid2) ==> 0
      parse("pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980\nhcl:#623a2f\n\neyr:2029 ecl:blu cid:129 byr:1989\niyr:2014 pid:896056539 hcl:#a97842 hgt:165cm\n\nhcl:#888785\nhgt:164cm byr:2001 iyr:2015 cid:88\npid:545766238 ecl:hzl\neyr:2022\n\niyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719")
        .count(isValid2) ==> 4
      theAnswerIs(parse(input).count(isValid2), 179)
    }
  }
}

