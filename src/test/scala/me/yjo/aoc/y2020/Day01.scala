package me.yjo.aoc.y2020

import me.yjo.util.readInts
import utest._

object Day01 extends DayBase {

  def pairThatSumsTo(values: Seq[Int], target: Int): (Int, Int) = {
    val valuesSet = values.toSet
    for {
      x <- valuesSet
      y = target - x
      if x < y && valuesSet.contains(y)
    } return (x, y)
    throw new IllegalArgumentException
  }

  val testNums = Seq(1721, 979, 366, 299, 675, 1456)

  def tripleThatSumsTo(values: Seq[Int], target: Int): (Int, Int, Int) = {
    val valuesSet = values.toSet
    for {
      x <- valuesSet
      y <- valuesSet
      if x < y
      z = target - x - y
      if y < z && valuesSet.contains(z)
    } return (x, y, z)
    throw new IllegalArgumentException
  }

  val tests = Tests {
    test("star1") {
      pairThatSumsTo(testNums, 2020) ==> (299, 1721)

      val pair = pairThatSumsTo(readInts(input), 2020)
      theAnswerIs(pair._1 * pair._2, 445536)
    }
    test("star2") {
      tripleThatSumsTo(testNums, 2020) ==> (366, 675, 979)

      val triple = tripleThatSumsTo(readInts(input), 2020)
      theAnswerIs(triple._1 * triple._2 * triple._3, 138688160)
    }
  }
}
