package me.yjo.aoc.y2020

import me.yjo.graph.{Area, Coord}
import utest.*

object Day03 extends DayBase {

  case class Grid(input: String) {
    val lines = input.linesIterator.toArray
    val area = Area(lines(0).length, lines.length)

    def apply(p: Coord) = lines(p.y)(p.x % area.width) == '#'

    def path(slope: Coord) = {
      Iterator.iterate(Coord.origin)(_ + slope)
        .takeWhile(_.y < area.height)
        .map(apply)
    }

    def count(slope: Coord): Long = path(slope).count(_ == true)
  }

  val tests = Tests {
    test("star1") {
      Grid(testInput).count(Coord(3, 1)) ==> 7
      theAnswerIs(Grid(input).count(Coord(3, 1)), 284)
    }
    test("star2") {
      val slopes = Seq(Coord(1, 1), Coord(3, 1), Coord(5, 1), Coord(7, 1), Coord(1, 2))
      slopes.map(Grid(testInput).count) ==> Seq(2, 7, 3, 4, 2)
      theAnswerIs(slopes.map(Grid(input).count).product, 3510149120L)
    }
  }
}
