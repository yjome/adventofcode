package me.yjo.aoc.y2020

import me.yjo.graph.CardinalDir.E
import me.yjo.graph.Coord.origin
import me.yjo.graph.{CardinalDir, Coord}
import me.yjo.util.*
import utest.*

object Day12 extends DayBase {

  override lazy val testInput = "F10\nN3\nF7\nR90\nF11"

  case class Ship(pos: Coord = origin, hdg: CardinalDir = E) {
    def step(instr: String) = instr match {
      case r"([NESW])${dir}([0-9]+)${IntString(units)}" =>
        copy(pos = pos.step(CardinalDir(dir(0)), units))
      case s"L${IntString(deg)}" => copy(hdg = hdg.leftDeg(deg))
      case s"R${IntString(deg)}" => copy(hdg = hdg.rightDeg(deg))
      case s"F${IntString(units)}" => copy(pos = pos.step(hdg, units))
    }
  }

  case class Ship2(pos: Coord = origin, wpt: Coord = Coord(10, -1)) {
    def step(instr: String) = instr match {
      case r"([NESW])${dir}([0-9]+)${IntString(units)}" =>
        copy(wpt = wpt.step(CardinalDir(dir(0)), units))
      case s"L${IntString(deg)}" => copy(wpt = wpt.rotateLeftDeg(deg))
      case s"R${IntString(deg)}" => copy(wpt = wpt.rotateRightDeg(deg))
      case s"F${IntString(units)}" => copy(pos = pos + wpt * units)
    }
  }

  def s1(in: String): Int =
    in.linesIterator.foldLeft(Ship())(_ step _).pos.manhattanDist(origin)

  def s2(in: String): Int =
    in.linesIterator.foldLeft(Ship2())(_ step _).pos.manhattanDist(origin)


  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 25
      theAnswerIs(s1(input), 1177)
    }
    test("star2") {
      s2(testInput) ==> 286
      theAnswerIs(s2(input), 46530)
    }
  }
}
