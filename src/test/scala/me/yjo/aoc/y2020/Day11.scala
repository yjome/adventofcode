package me.yjo.aoc.y2020

import me.yjo.graph.{Area, Coord}
import utest.*

import scala.annotation.tailrec

object Day11 extends DayBase {

  case class Grid(area: Area, seats: Map[Coord, Boolean]) {
    def apply(coord: Coord) = seats.getOrElse(coord, false)

    def next = Grid(area, seats.map {
      case (coord, false) => (coord, !coord.neighbours8Way.exists(apply))
      case (coord, true) => (coord, coord.neighbours8Way.count(apply) < 4)
    })

    @tailrec
    final def runUntilStatic(): Grid = {
      val next = this.next
      if (this == next) this else next.runUntilStatic()
    }

    def visibleSeat(from: Coord, dir: Coord): Option[Boolean] =
      Iterator.iterate(from)(_ + dir).drop(1)
        .takeWhile(area.contains)
        .collectFirst(seats)

    def occupiedSeatsFrom(from: Coord): Int = (for {
      dir <- Coord.origin.neighbours8Way
      occupied <- visibleSeat(from, dir)
      if occupied
    } yield ()).size

    def next2 = Grid(area, seats.map {
      case (coord, false) => (coord, occupiedSeatsFrom(coord) == 0)
      case (coord, true) => (coord, occupiedSeatsFrom(coord) < 5)
    })

    @tailrec
    final def runUntilStatic2(): Grid = {
      val next = this.next2
      if (this == next) this else next.runUntilStatic2()
    }

    def count = seats.values.count(x => x)

    override def toString = area.render(coord => seats.get(coord) match {
      case Some(true) => '#'
      case Some(false) => 'L'
      case None => '.'
    })
  }

  object Grid {
    def apply(string: String): Grid = {
      val map = Area.unRender(string)
        .filter { case (_, c) => c != '.' }
        .map { case (coord, char) => (coord, char == '#') }
        .toMap
      Grid(Area.rangeOf(map.keys), map)
    }
  }

  def s1(input: String): Int = {
    Grid(input).runUntilStatic().count
  }

  def s2(input: String) = {
    Grid(input).runUntilStatic2().count
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 37
      theAnswerIs(s1(input), 2254)
    }
    test("star2") {
      s2(testInput) ==> 26
      theAnswerIs(s2(input), 2004)
    }
  }
}
