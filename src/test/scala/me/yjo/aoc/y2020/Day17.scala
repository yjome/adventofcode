package me.yjo.aoc.y2020

import me.yjo.graph.{Area, Coord}
import me.yjo.util.{counts, nTimes}
import utest.*

import scala.collection.Iterable

object Day17 extends DayBase {

  case class Coord4(x: Int, y: Int, z: Int, w: Int = 0) {
    def neighbours3D: Iterable[Coord4] = for {
      nx <- x - 1 to x + 1
      ny <- y - 1 to y + 1
      nz <- z - 1 to z + 1
      if nx != x || ny != y || nz != z
    } yield Coord4(nx, ny, nz)

    def neighbours4D: Iterable[Coord4] = for {
      nx <- x - 1 to x + 1
      ny <- y - 1 to y + 1
      nz <- z - 1 to z + 1
      nw <- w - 1 to w + 1
      if nx != x || ny != y || nz != z || nw != w
    } yield Coord4(nx, ny, nz, nw)
  }

  case class State(active: Set[Coord4]) {
    def step3D: State = State((for {
      (coord, count) <- counts(active.view.flatMap(_.neighbours3D))
      if count == 3 || count == 2 && active(coord)
    } yield coord).toSet)

    def step4D: State = State((for {
      (coord, count) <- counts(active.view.flatMap(_.neighbours4D))
      if count == 3 || count == 2 && active(coord)
    } yield coord).toSet)
  }

  object State {
    def apply(in: String): State = {
      State((for {
        (Coord(x, y), char) <- Area.unRender(in)
        if char == '#'
      } yield Coord4(x, y, 0)).toSet)
    }
  }

  def s1(input: String): Long = {
    nTimes[State](6, _.step3D, State(input)).active.size
  }

  def s2(input: String): Long = {
    nTimes[State](6, _.step4D, State(input)).active.size
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 112
      theAnswerIs(s1(input), 315)
    }
    test("star2") {
      s2(testInput) ==> 848
      theAnswerIs(s2(input), 1520)
    }
  }
}
