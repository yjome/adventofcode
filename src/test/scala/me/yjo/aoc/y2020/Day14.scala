package me.yjo.aoc.y2020

import me.yjo.util.LongString
import utest._

import scala.collection.{immutable, mutable}

object Day14 extends DayBase {

  def parseBin(bin: String): Long = java.lang.Long.parseLong(bin, 2)

  case class Mask(mask: String) {
    val andMask = parseBin(mask.replace('X', '1'))
    val orMask = parseBin(mask.replace('X', '0'))

    def apply(in: Long): Long = in & andMask | orMask
  }

  def s1(input: String): Long = {
    var mask = Mask("X" * 36)
    val mem = mutable.Map[Long, Long]()
    for (line <- input.linesIterator) line match {
      case s"mask = ${maskPattern}" => mask = Mask(maskPattern)
      case s"mem[${LongString(addr)}] = ${LongString(value)}" =>
        mem(addr) = mask(value)
    }
    mem.values.sum
  }

  case class Mask2(mask: String) {
    val andMask = parseBin(mask.replace('0', '1').replace('X', '0'))
    val orMask = parseBin(mask.replace('X', '0'))
    val floating = parseBin(mask.replace('1', '0').replace('X', '1'))

    def apply(in: Long): Iterator[Long] = {
      for (
        floaters <- immutable.BitSet.fromBitMask(Array(floating)).subsets()
      ) yield in & andMask | orMask | floaters.toBitMask(0)
    }
  }

  def s2(input: String): Long = {
    var mask = Mask2("0" * 36)
    val mem = mutable.Map[Long, Long]()
    for (line <- input.linesIterator) line match {
      case s"mask = ${maskPattern}" => mask = Mask2(maskPattern)
      case s"mem[${LongString(addr)}] = ${LongString(value)}" =>
        for (maskedAddr <- mask(addr))
          mem(maskedAddr) = value
    }
    mem.values.sum
  }

  val tests = Tests {
    test("star1") {
      Mask("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X")(11) ==> 73
      s1(testInput) ==> 165
      theAnswerIs(s1(input), 7440382076205L)
    }
    test("star2") {
      Mask2("000000000000000000000000000000X1001X")(42).toSet ==> Set(26, 27, 58, 59)
      s2(
        """mask = 000000000000000000000000000000X1001X
          |mem[42] = 100
          |mask = 00000000000000000000000000000000X0XX
          |mem[26] = 1""".stripMargin
      ) ==> 208
      theAnswerIs(s2(input), 4200656704538L)
    }
  }
}
