package me.yjo.aoc.y2020

import utest._

object Day15 extends DayBase {

  def game(indexes: Map[Int, Int], prev: Int, index: Int): LazyList[Int] = {
    val next = indexes.get(prev).map(index - 1 - _).getOrElse(0)
    next #:: game(indexes.updated(prev, index - 1), next, index + 1)
  }

  def game(initial: Int*): LazyList[Int] = {
    val lastIndexes = initial.dropRight(1).zipWithIndex.toMap
    val last = initial.last
    LazyList.from(initial) #::: game(lastIndexes, last, initial.size)
  }

  val tests = Tests {
    test("star1") {
      game(0, 3, 6).take(10) ==> Seq(0, 3, 6, 0, 3, 3, 1, 0, 4, 0)
      game(0, 3, 6)(2020 - 1) ==> 436
      theAnswerIs(game(6, 19, 0, 5, 7, 13, 1)(2020 - 1), 468)
    }
    test("star2") {
      theAnswerIs(game(6, 19, 0, 5, 7, 13, 1)(30000000 - 1), 1801753)
    }
  }
}
