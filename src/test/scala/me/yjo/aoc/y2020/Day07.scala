package me.yjo.aoc.y2020

import me.yjo.util._
import utest._

import scala.collection.mutable

object Day07 extends DayBase {

  def collectCounts(iter: Iterable[(String, Int)]): Map[String, Int] = {
    val counts = mutable.Map[String, Int]()
    for ((k, v) <- iter) {
      counts(k) = counts.getOrElse(k, 0) + v
    }
    counts.toMap
  }

  case class BagRule(colour: String, contents: Map[String, Int]) {
    def deepContents(rulesByColour: Map[String, BagRule]): Map[String, Int] = {
      collectCounts(contents.toSeq.flatMap { case colour -> count =>
        ((colour, 1) :: rulesByColour(colour).deepContents(rulesByColour).toList)
          .map { case k -> v => k -> v * count }
      })}
  }

  case object BagRule {
    def apply(line: String): BagRule = line match {
      case r"(\w+ \w+)${colour} bags contain (.*)${rest}\." => BagRule(colour, parseContents(rest))
    }

    def parseContents(rest: String): Map[String, Int] = rest match {
      case "no other bags" => Map()
      case _ => rest.split(", ").map {
        case r"(\d+)${IntString(count)} (\w+ \w+)${colour} bags?" => colour -> count
      }.toMap
    }
  }

  def s1(input: String) = {
    val rules = input.linesIterator.map(BagRule.apply).map(r => r.colour -> r).toMap
    rules.values.count(_.deepContents(rules).contains("shiny gold"))
  }

  def s2(input: String) = {
    val rules = input.linesIterator.map(BagRule.apply).map(r => r.colour -> r).toMap
    rules("shiny gold").deepContents(rules).values.sum
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 4
      theAnswerIs(s1(input), 131)
    }
    test("star2") {
      s2(testInput) ==> 32
      theAnswerIs(s2(input), 11261)
    }
  }
}
