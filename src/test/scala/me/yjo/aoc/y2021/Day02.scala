package me.yjo.aoc.y2021

import me.yjo.util.*
import utest.*

object Day02 extends DayBase {

  override lazy val testInput = "forward 5\ndown 5\nforward 8\nup 3\ndown 8\nforward 2"

  case class Sub(x: Int = 0, d: Int = 0) {
    def step(instr: String): Sub = instr match {
      case s"forward ${IntString(dx)}" => copy(x = x + dx)
      case s"down ${IntString(dd)}" => copy(d = d + dd)
      case s"up ${IntString(dd)}" => copy(d = d - dd)
    }
  }

  def s1(input: String): Long = {
    val endState = input.linesIterator.foldLeft(Sub())(_ step _)
    endState.x * endState.d
  }

  case class Sub2(x: Int = 0, d: Int = 0, aim: Int = 0) {
    def step(instr: String): Sub2 = instr match {
      case s"forward ${IntString(dx)}" => copy(x = x + dx, d = d + aim * dx)
      case s"down ${IntString(dAim)}" => copy(aim = aim + dAim)
      case s"up ${IntString(dAim)}" => copy(aim = aim - dAim)
    }
  }

  def s2(input: String): Long = {
    val endState = input.linesIterator.foldLeft(Sub2())(_ step _)
    endState.x * endState.d
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 150
      theAnswerIs(s1(input), 1580000)
    }
    test("star2") {
      s2(testInput) ==> 900
      theAnswerIs(s2(input), 1251263225)
    }
  }
}

