package me.yjo.aoc.y2021

import me.yjo.graph.{Area, Coord}
import me.yjo.util.*
import utest.*

object Day25 extends DayBase {
  case class Cukes(field: Map[Coord, Char], area: Area) {
    def eastOf(c: Coord) = Coord((c.x + 1) % area.width, c.y)
    def southOf(c: Coord) = Coord(c.x, (c.y + 1) % area.height)
    def next: Cukes = {
      val afterEasts = field.map {
        case c -> '>' if !field.contains(eastOf(c)) => eastOf(c) -> '>'
        case entry => entry
      }
      Cukes(afterEasts.map {
        case c -> 'v' if !afterEasts.contains(southOf(c)) => southOf(c) -> 'v'
        case entry => entry
      }, area)
    }
    override def toString = area.render(field.getOrElse(_, '.'))
  }

  def s1(input: String): Long = {
    val cukes = Cukes(Area.unRender(input).filter(">v" contains _._2).toMap, Area.rangeOf(input))
    val states = LazyList.iterate(cukes)(_.next)
    states.tail.zip(states).indexWhere(_ == _) + 1
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 58
      theAnswerIs(s1(input), 374)
    }
  }
}

