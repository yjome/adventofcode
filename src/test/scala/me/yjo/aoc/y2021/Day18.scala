package me.yjo.aoc.y2021

import me.yjo.aoc.y2020.Day18.Calculator.rep
import me.yjo.util.*
import utest.*

import java.util.Optional
import scala.annotation.tailrec
import scala.util.parsing.combinator.RegexParsers

object Day18 extends DayBase {

  case class Explosion(newNode: SNum, lExp: Int, rExp: Int)

  sealed trait SNum {
    def magnitude: Long
    def +(other: SNum) = Pair(this, other).reduce()
    def addLeft(x: Int): SNum
    def addRight(x: Int): SNum
    def explode(depth: Int = 0): Option[Explosion]
    def split(): Option[SNum]

    @tailrec
    final def reduce(): SNum =
      explode().map(_.newNode).orElse(split()) match {
        case Some(updated) => updated.reduce()
        case None => this
      }
  }

  object SNum extends RegexParsers {
    def leaf: Parser[Leaf] = """\d+""".r ^^ { n => Leaf(n.toInt) }
    def pair: Parser[Pair] = '[' ~> sNum ~ ',' ~ sNum <~ ']' ^^ { case l ~ _ ~ r => Pair(l, r) }
    def sNum: Parser[SNum] = leaf | pair

    def apply(str: String): SNum = parse(sNum, str).get
  }

  case class Leaf(n: Int) extends SNum {
    override def toString = n.toString
    override def magnitude = n
    override def addLeft(x: Int) = if x == 0 then this else Leaf(n + x)
    override def addRight(x: Int) = if x == 0 then this else Leaf(n + x)
    override def split(): Option[SNum] = if n >= 10 then Some(Pair(n / 2, (n + 1) / 2)) else None
    override def explode(depth: Int = 0): Option[Explosion] = None
  }

  given Conversion[Int, Leaf] = Leaf.apply

  case class Pair(l: SNum, r: SNum) extends SNum {
    override def toString = f"[$l,$r]"
    override def magnitude = 3 * l.magnitude + 2 * r.magnitude
    override def addLeft(x: Int) = if x == 0 then this else Pair(l.addLeft(x), r)
    override def addRight(x: Int) = if x == 0 then this else Pair(l, r.addRight(x))

    override def split(): Option[SNum] =
      l.split().map(newL => Pair(newL, r)).orElse(r.split().map(newR => Pair(l, newR)))

    override def explode(depth: Int = 0): Option[Explosion] =
      if depth == 4 then
        val Pair(Leaf(ln), Leaf(rn)) = this
        Some(Explosion(0, ln, rn))
      else
        l.explode(depth + 1) match {
          case Some(Explosion(newL, lExp, rExp)) => Some(Explosion(Pair(newL, r.addLeft(rExp)), lExp, 0))
          case None => r.explode(depth + 1) match {
            case Some(Explosion(newR, lExp, rExp)) => Some(Explosion(Pair(l.addRight(lExp), newR), 0, rExp))
            case None => None
          }
        }
  }

  def s1(input: String): Long = {
    input.linesIterator.map(SNum.apply).reduceLeft(_ + _).magnitude
  }

  def s2(input: String): Long = {
    val nums = input.linesIterator.map(SNum.apply).toList
    (for {x <- nums; y <- nums; if x != y} yield (x + y).magnitude).max
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 4140
      theAnswerIs(s1(input), 4457)
    }
    test("star2") {
      s2(testInput) ==> 3993
      theAnswerIs(s2(input), 4784)
    }
  }
}

