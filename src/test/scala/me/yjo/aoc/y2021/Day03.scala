package me.yjo.aoc.y2021

import me.yjo.util.*
import utest.*

import java.lang.Long.parseLong
import scala.annotation.tailrec

object Day03 extends DayBase {

  override lazy val testInput = "00100\n11110\n10110\n10111\n10101\n01111\n00111\n11100\n10000\n11001\n00010\n01010"

  def testBit(value: Int, bit: Int): Boolean =
    (value & 1 << bit) != 0

  def binaryToInt(bits: Seq[Boolean]): Long = bits match {
    case Seq() => 0
    case Seq(false, tail*) => 2 * binaryToInt(tail)
    case Seq(true, tail*) => 1 + 2 * binaryToInt(tail)
  }

  def popularBit(values: Seq[Int], bit: Int): Boolean = {
    values.count(x => testBit(x, bit)) >= (values.length + 1) / 2
  }

  def s1(input: String): Long = {
    val numBits = input.linesIterator.next().length
    val nums = input.linesIterator.map(Integer.parseInt(_, 2)).toSeq

    val mostPopularBits = (0 until numBits).map(b => popularBit(nums, b))

    val gamma = binaryToInt(mostPopularBits)
    val epsilon = binaryToInt(mostPopularBits.map(!_))
    gamma * epsilon
  }

  @tailrec
  def findRating(values: Seq[Int], wantPopular: Boolean, currentBit: Int): Long = values match {
    case Seq(value) => value
    case _ =>
      assert(currentBit >= 0)
      val popular = popularBit(values, currentBit)
      val wanted = if wantPopular then popular else !popular
      findRating(values.filter(x => testBit(x, currentBit) == wanted), wantPopular, currentBit - 1)
  }

  def s2(input: String): Long = {
    val numBits = input.linesIterator.next().length
    val nums = input.linesIterator.map(Integer.parseInt(_, 2)).toSeq

    val oxy = findRating(nums, true, numBits - 1)
    val co2 = findRating(nums, false, numBits - 1)
    oxy * co2
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 198
      theAnswerIs(s1(input), 2954600)
    }
    test("star2") {
      s2(testInput) ==> 230
      theAnswerIs(s2(input), 1662846)
    }
  }
}

