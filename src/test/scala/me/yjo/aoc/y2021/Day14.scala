package me.yjo.aoc.y2021

import me.yjo.util.*
import utest.*

import scala.collection.MapView

object Day14 extends DayBase {

  type PairCounts = Map[(Char, Char), Long]
  type InsertionRules = Map[(Char, Char), Char]

  def parse(input: String): (PairCounts, InsertionRules) = {
    val Array(seed, mappingStr) = input.split("\n\n")
    val seedCounts = counts(pairs(seed + '.')).toMap
    val mapping = mappingStr.linesIterator.map(line => (line(0), line(1)) -> line(6)).toMap
    (seedCounts, mapping)
  }

  def step(rules: InsertionRules)(counts: PairCounts): PairCounts = {
    val newCounts = (for {
      (matchPair, insertion) <- rules.view
      count <- counts.get(matchPair).toList
      newPair <- Iterator((matchPair._1, insertion), (insertion, matchPair._2))
    } yield newPair -> count).groupBy(_._1).view.mapValues(_.map(_._2).sum).toMap ++ counts.filter(_._1._2 == '.')
    newCounts
  }

  def run(input: String, iterations: Int): Long = {
    val (counts, rules) = parse(input)
    val endCounts = nTimes(iterations, step(rules), counts)
    val charCounts = endCounts.toSeq.groupBy(_._1._1).view.mapValues(_.map(_._2).sum).filter(_._2 > 0).toMap
    charCounts.values.max - charCounts.values.min
  }

  def s1(input: String): Long = run(input, 10)

  def s2(input: String): Long = run(input, 40)

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 1588
      theAnswerIs(s1(input), 2915)
    }
    test("star2") {
      s2(testInput) ==> 2188189693529L
      theAnswerIs(s2(input), 3353146900153L)
    }
  }
}

