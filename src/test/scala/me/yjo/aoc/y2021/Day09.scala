package me.yjo.aoc.y2021

import me.yjo.graph.{Area, Coord}
import me.yjo.util.*
import utest.*

import scala.collection.mutable

object Day09 extends DayBase {

  override lazy val testInput = "2199943210\n3987894921\n9856789892\n8767896789\n9899965678"

  case class Grid(input: String) extends PartialFunction[Coord, Int] {
    val lines = input.linesIterator.toArray
    val area = Area(lines(0).length, lines.length)

    override def isDefinedAt(c: Coord) = area.contains(c)

    override def apply(c: Coord) = lines(c.y)(c.x) - '0'

    lazy val lowPoints: Set[Coord] = (for {
      p <- area
      value = this (p)
      neighbours = p.neighbours4Way.collect(this)
      if neighbours.forall(_ > value)
    } yield p).toSet

    def basins(): List[Set[Coord]] = {
      lazy val lowPointFrom: Coord => Coord = memoise(p =>
        if lowPoints contains p then p else {
          val lowNeighbour = p.neighbours4Way.find(n => isDefinedAt(n) && this (n) < this (p)).get
          lowPointFrom(lowNeighbour)
        }
      )
      area.filter(p => this (p) < 9).groupBy(lowPointFrom).values.map(_.toSet).toList
    }
  }

  def s1(input: String): Long = {
    val grid = Grid(input)
    grid.lowPoints.view.map(p => grid(p) + 1).sum
  }

  def s2(input: String): Long = {
    val grid = Grid(input)
    grid.basins().sortBy(-_.size).take(3).map(_.size).product
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 15
      theAnswerIs(s1(input), 508)
    }
    test("star2") {
      s2(testInput) ==> 1134
      theAnswerIs(s2(input), 1564640)
    }
  }
}

