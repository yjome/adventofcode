package me.yjo.aoc.y2021

import me.yjo.graph.{Area, Coord}
import me.yjo.util.*
import utest.*

import scala.collection.mutable

object Day11 extends DayBase {

  val cavern = Area(10, 10)

  case class Grid(input: String) {
    val lines = input.linesIterator.toArray

    def apply(p: Coord): Int = lines(p.y)(p.x) - '0'

    def flashes = input.count(_ == '0')

    def step(): Grid = {
      val allFlashes = mutable.Set[Coord]()
      var newFlashes = cavern.filter(p => apply(p) == 9).toSet

      while {
        allFlashes ++= newFlashes
        newFlashes = newFlashes.flatMap(_.neighbours8Way).filter(p => cavern.contains(p)
          && !allFlashes.contains(p)
          && (apply(p) + 1 + p.neighbours8Way.count(allFlashes.contains)) > 9
        )
        newFlashes.nonEmpty
      } do ()

      Grid(cavern.render(p => {
        if allFlashes.contains(p) then '0' else
          ('0' + (apply(p) + 1 + p.neighbours8Way.count(allFlashes.contains))).toChar
      }))
    }

    val steps = LazyList.iterate(this)(_.step())
  }

  def s1(input: String): Long = {
    Grid(input).steps.take(101).map(_.flashes).sum
  }

  def s2(input: String): Long = {
    Grid(input).steps.indexWhere(_.flashes == 100)
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 1656
      theAnswerIs(s1(input), 1644)
    }
    test("star2") {
      s2(testInput) ==> 195
      theAnswerIs(s2(input), 229)
    }
  }
}

