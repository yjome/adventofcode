package me.yjo.aoc.y2021

import me.yjo.graph.{Area, Coord, Search}
import me.yjo.maths.ExactMathsImplicits.IntIsExactIntegral
import me.yjo.stringUtils
import me.yjo.stringUtils.bold
import me.yjo.util.*
import utest.*

import scala.collection.immutable.ArraySeq
import scala.math.Integral

object Day15 extends DayBase {

  class Grid(input: String) {
    val lines = ArraySeq.from(input.linesIterator)
    val area = Area.rangeOf(input)

    def charAt(p: Coord): Char = lines(p.y)(p.x)
    final def apply(p: Coord): Int = charAt(p) - '0'

    def neighboursAndHopCosts(p: Coord): Iterable[(Coord, Int)] =
      p.neighbours4Way.filter(area.contains).map(n => (n, apply(n)))

    def minRoute = {
      val dest = area.bottomRight
      Search.dijkstra(area.topLeft, dest, neighboursAndHopCosts).get
    }
  }

  def s1(input: String): Long = {
    val grid = Grid(input)
    val route = grid.minRoute
    println(route.renderBold(grid.area, grid.charAt) + "\n")
    route.totalDist
  }

  class Grid2(input: String) extends Grid(input) {
    val tileArea = Area.rangeOf(input)
    override val area = Area(tileArea.width * 5, tileArea.height * 5)

    override def charAt(p: Coord) = {
      val gridOffset = p.x / tileArea.width + p.y / tileArea.height
      val tileCoord = Coord(p.x % tileArea.width, p.y % tileArea.height)
      val baseRisk = super.charAt(tileCoord) + gridOffset
      ((baseRisk - '1') % 9 + '1').toChar
    }
  }

  def s2(input: String): Long = {
    val grid = Grid2(input)
    val route = grid.minRoute
    println(route.renderBold(grid.area, grid.charAt) + "\n")
    route.totalDist
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 40
      theAnswerIs(s1(input), 398)
    }
    test("star2") {
      s2(testInput) ==> 315
      theAnswerIs(s2(input), 2817)
    }
  }
}

