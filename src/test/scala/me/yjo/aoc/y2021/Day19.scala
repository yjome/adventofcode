package me.yjo.aoc.y2021

import me.yjo.util.*
import utest.*

import java.lang.Math.{abs, max, min}
import scala.annotation.tailrec
import scala.collection.{MultiDict, MultiSet}

object Day19 extends DayBase {

  case class Coord(x: Int, y: Int, z: Int) {
    def +(other: Coord): Coord = Coord(x + other.x, y + other.y, z + other.z)
    def -(other: Coord): Coord = Coord(x - other.x, y - other.y, z - other.z)
    def canonicalise = {
      val dims = Array(abs(x), abs(y), abs(z)).sortInPlace()
      Coord(dims(0), dims(1), dims(2))
    }
    def dist(other: Coord): Int = abs(x - other.x) + abs(y - other.y) + abs(z - other.z)
  }

  object Coord {
    val orientations: Seq[Coord => Coord] = Seq(
      c => Coord(c.x, c.y, c.z), c => Coord(c.x, -c.y, -c.z), c => Coord(-c.x, c.y, -c.z), c => Coord(-c.x, -c.y, c.z),
      c => Coord(c.x, c.z, -c.y), c => Coord(c.x, -c.z, c.y), c => Coord(-c.x, c.z, c.y), c => Coord(-c.x, -c.z, -c.y),
      c => Coord(c.y, c.x, -c.z), c => Coord(c.y, -c.x, c.z), c => Coord(-c.y, c.x, c.z), c => Coord(-c.y, -c.x, -c.z),
      c => Coord(c.y, c.z, c.x), c => Coord(c.y, -c.z, -c.x), c => Coord(-c.y, c.z, -c.x), c => Coord(-c.y, -c.z, c.x),
      c => Coord(c.z, c.x, c.y), c => Coord(c.z, -c.x, -c.y), c => Coord(-c.z, c.x, -c.y), c => Coord(-c.z, -c.x, c.y),
      c => Coord(c.z, c.y, -c.x), c => Coord(c.z, -c.y, c.x), c => Coord(-c.z, c.y, c.x), c => Coord(-c.z, -c.y, -c.x),
    )
  }

  class ScannerField(val pings: Set[Coord], val scanners: Set[Coord] = Set(Coord(0, 0, 0))) {
    val diffs = MultiDict.from(for {
      p1 <- pings.iterator
      p2 <- pings.iterator
      if p1 != p2
      diff = p2 - p1
    } yield diff -> p1)
    lazy val canonicalDiffs = MultiSet.from(diffs.iterator.map(_._1.canonicalise))

    def merge(that: ScannerField, expectedCommon: Int = 12): Option[ScannerField] = {
      val expectedCommonDiffs = expectedCommon * (expectedCommon - 1)
      if canonicalDiffs.occurrences.map({ case (d, n) => min(n, that.canonicalDiffs.get(d)) }).sum < expectedCommonDiffs then
        return None
      (for {
        oThat <- that.orientations.iterator
        commonDiffs = diffs.keySet.intersect(oThat.diffs.keySet).view
        if commonDiffs.map(d => max(diffs.get(d).size, oThat.diffs.get(d).size)).sum >= expectedCommonDiffs
        diff <- commonDiffs
        p1 <- diffs.get(diff)
        p2 <- oThat.diffs.get(diff)
        delta = p2 - p1
        if pings.map(_ + delta).intersect(oThat.pings).sizeIs >= expectedCommon
      } yield ScannerField(pings ++ oThat.pings.map(_ - delta), scanners ++ oThat.scanners.map(_ - delta))).nextOption()
    }

    val orientations = LazyList.from(Coord.orientations.iterator.map(o => ScannerField(pings.map(o), scanners.map(o))))
  }

  def parse(input: String): List[ScannerField] =
    input.split("\n\n--- scanner \\d+ ---\n")
      .map(str => ScannerField(str.linesIterator.map {
        case s"${IntString(x)},${IntString(y)},${IntString(z)}" => Coord(x, y, z)
      }.toSet)).toList

  @tailrec
  def merge(scanners: List[ScannerField]): ScannerField =
    if scanners.sizeIs == 1 then scanners.head else
      merge((for {
        s1 <- scanners.iterator
        s2 <- scanners.iterator
        if s2 != s1
        merged <- s1 merge s2
      } yield merged :: scanners.filter(s => s != s1 && s != s2)).next())

  val parseAndMerge: String => ScannerField = memoise(input => merge(parse(input)))

  def s1(input: String): Long = {
    parseAndMerge(input).pings.size
  }

  def s2(input: String): Long = {
    val scanners = parseAndMerge(input).scanners
    (for {s1 <- scanners; s2 <- scanners} yield s1 dist s2).max
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 79
      theAnswerIs(s1(input), 445)
    }
    test("star2") {
      s2(testInput) ==> 3621
      theAnswerIs(s2(input), 13225)
    }
  }
}
