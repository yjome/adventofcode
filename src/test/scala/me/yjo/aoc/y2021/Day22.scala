package me.yjo.aoc.y2021

import me.yjo.util.*
import utest.*

import scala.annotation.tailrec
import scala.collection.mutable

object Day22 extends DayBase {

  def rangesOverlap(r1: Range, r2: Range): Boolean =
    r1.contains(r2.head) || r2.contains(r1.head)

  // Divides r2 into non-empty ranges before (excl), inside (incl) & after (excl) r1
  def split(r1: Range, r2: Range): Seq[Range] =
    if !rangesOverlap(r1, r2) || r1.contains(r2.head) && r1.contains(r2.last) then Seq(r2) else
      Seq(
        r2.head until r1.head,
        r2.head.max(r1.head) to r1.last.min(r2.last),
        (r1.last + 1) to r2.last
      ).filter(_.nonEmpty)

  case class Cube(x: Range, y: Range, z: Range) {
    require(x.step == 1 && y.step == 1 && z.step == 1)
    def contains(p: (Int, Int, Int)): Boolean = x.contains(p._1) && y.contains(p._2) && z.contains(p._3)

    def contains(that: Cube): Boolean =
      contains((that.x.head, that.y.head, that.z.head)) && contains((that.x.last, that.y.last, that.z.last))

    def size: Long = x.size.toLong * y.size.toLong * z.size.toLong

    def intersects(that: Cube): Boolean =
      rangesOverlap(this.x, that.x) && rangesOverlap(this.y, that.y) && rangesOverlap(this.z, that.z)

    def -(that: Cube): Seq[Cube] = {
      for {
        xr <- split(that.x, this.x)
        yr <- split(that.y, this.y)
        zr <- split(that.z, this.z)
        cube = Cube(xr, yr, zr)
        if !that.contains(cube)
      } yield cube
    }
    override def toString = s"Cube(${x.head} to ${x.last}, ${y.head} to ${y.last}, ${z.head} to ${z.last})"
  }

  object Cube {
    def unapply(string: String): Option[Cube] = string match {
      case s"x=${IntString(xMin)}..${IntString(xMax)},y=${IntString(yMin)}..${IntString(yMax)},z=${IntString(zMin)}..${IntString(zMax)}" =>
        Some(Cube(xMin to xMax, yMin to yMax, zMin to zMax))
      case _ => None
    }
  }

  def parse(input: String): IndexedSeq[(Cube, Boolean)] = {
    input.linesIterator.map(line => {
      val Array(onOrOff, Cube(cube)) = line.split(' ')
      (cube, onOrOff == "on")
    }).toIndexedSeq
  }

  def s1(input: String): Long = {
    val stepsReversed = parse(input).reverse
    val testRegion = for (x <- -50 to 50; y <- -50 to 50; z <- -50 to 50) yield (x, y, z)
    testRegion.count(p => stepsReversed.find(step => step._1.contains(p)).exists(_._2))
  }

  case class Volume(cubes: Set[Cube] = Set()) {
    def +(cube: Cube): Volume = this ++ Seq(cube)
    def -(cube: Cube): Volume = this -- Seq(cube)
    def contains(p: (Int, Int, Int)): Boolean = cubes.exists(_.contains(p))

    @tailrec
    final def ++(newCubes: Seq[Cube]): Volume = {
      if newCubes.isEmpty then return this
      val Seq(newCube, otherNewCubes*) = newCubes
      cubes.find(newCube.intersects) match {
        case None => Volume(cubes + newCube) ++ otherNewCubes
        case Some(intersecting) if newCube == intersecting => this ++ otherNewCubes
        case Some(intersecting) => this ++ ((newCube - intersecting) ++ otherNewCubes)
      }
    }

    @tailrec
    final def --(cubesToRemove: Seq[Cube]): Volume = {
      if cubesToRemove.isEmpty then return this
      val Seq(cubeToRemove, otherCubesToRemove*) = cubesToRemove
      cubes.find(cubeToRemove.intersects) match {
        case None => this -- otherCubesToRemove
        case Some(intersecting) if cubeToRemove == intersecting => Volume(cubes - intersecting) -- otherCubesToRemove
        case Some(intersecting) =>
          (Volume(cubes - intersecting) ++ (intersecting - cubeToRemove)) -- cubesToRemove
      }
    }

    def size: Long = cubes.view.map(_.size).sum
  }

  def s2(input: String): Long = {
    val volume = parse(input).foldLeft(Volume()) {
      case (acc, (cube, true)) => acc + cube
      case (acc, (cube, false)) => acc - cube
    }
    volume.size
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 590784
      theAnswerIs(s1(input), 533863)
    }
    test("star2") {
      s2(readDataFile("Day22.test2.txt")) ==> 2758514936282235L
      theAnswerIs(s2(input), 1261885414840992L)
    }
  }
}
