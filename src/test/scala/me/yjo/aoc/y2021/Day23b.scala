package me.yjo.aoc.y2021

import me.yjo.aoc.y2021.Day23b.Burrow.{destBurrow, plan}
import me.yjo.graph.{Area, Coord, Search}
import me.yjo.util.*
import utest.*

import scala.collection.immutable.MultiDict

object Day23b extends DayBase {
  val dest =
    """#############
      |#..~.~.~.~..#
      |###A#B#C#D###
      |  #A#B#C#D#
      |  #A#B#C#D#
      |  #A#B#C#D#
      |  #########""".stripMargin

  def podMoveCost(pod: Char) = pod match {
    case 'A' => 1
    case 'B' => 10
    case 'C' => 100
    case 'D' => 1000
  }

  inline def isHallway(c: Coord): Boolean = c.y == 1

  def range(x: Int, y: Int): Range = x.min(y) to x.max(y)

  case class Burrow(podMap: Map[Coord, Char]) {
    def moves(podType: Char, c: Coord): Seq[(Coord, Int)] = {
      val homeRoom = plan.get(podType)
      val isHomeTime = homeRoom.collect(podMap).forall(_ == podType)
      if (homeRoom.contains(c) && isHomeTime)
        return Seq() // already home
      val toRoomMoves =
        if !isHallway(c) || !isHomeTime then Nil else {
          val target = homeRoom.filter(!podMap.contains(_)).maxBy(_.y)
          if range(c.x, target.x).map(Coord(_, 1)).forall(step => step == c || !podMap.contains(step)) then
            Seq(target)
          else Nil
        }
      val toHallMoves =
        if isHallway(c) || (2 until c.y).exists(y => podMap.contains(Coord(c.x, y))) then Nil else
          plan.get('.').view.filter(target => range(c.x, target.x).forall(x => !podMap.contains(Coord(x, 1)))).toList

      (toRoomMoves ++ toHallMoves).map(target => (target, c.manhattanDist(target) * podMoveCost(podType)))
    }
    def moves: Seq[(Burrow, Int)] = (for {
      (c, pod) <- podMap.view
      (target, cost) <- moves(pod, c)
    } yield (Burrow(podMap - c + (target -> pod)), cost)).toSeq

    def solve = Search.dijkstra[Burrow, Int](this, destBurrow, _.moves).get
  }

  object Burrow {
    val plan = MultiDict.from(Area.unRender(dest).map(_.swap))
    val destBurrow = parse(dest)
    def parse(input: String): Burrow = Burrow(Area.unRender(input).filter(_._2.isUpper).toMap)
  }

  def s2(input: String): Long = {
    Burrow.parse(input).solve.totalDist
  }

  val tests = Tests {
    test("star2") {
      s2(testInput) ==> 44169
      theAnswerIs(s2(input), 46451)
    }
  }
}

