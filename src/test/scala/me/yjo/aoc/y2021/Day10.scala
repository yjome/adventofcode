package me.yjo.aoc.y2021

import me.yjo.util.*
import utest.*

import scala.annotation.tailrec

object Day10 extends DayBase {

  enum Bracket(val open: Char, val close: Char, val syntaxScore: Int, val autocompleteScore: Int) {
    case Round extends Bracket('(', ')', 3, 1)
    case Square extends Bracket('[', ']', 57, 2)
    case Curly extends Bracket('{', '}', 1197, 3)
    case Angle extends Bracket('<', '>', 25137, 4)
  }

  object OpenBracket:
    def unapply(c: Char): Option[Bracket] = Bracket.values.find(_.open == c)

  object CloseBracket:
    def unapply(c: Char): Option[Bracket] = Bracket.values.find(_.close == c)

  @tailrec
  def score1(line: List[Char], bracketStack: List[Bracket] = Nil): Int = (line, bracketStack) match {
    case (Nil, _) => 0
    case (OpenBracket(b) :: rest, stack) => score1(rest, b :: stack)
    case (CloseBracket(b) :: rest, head :: stack) if head == b => score1(rest, stack)
    case (CloseBracket(badBracket) :: _, _) => badBracket.syntaxScore
  }

  def s1(input: String): Long = {
    input.linesIterator.map(line => score1(line.toList)).sum
  }

  @tailrec
  def score2(line: List[Char], bracketStack: List[Bracket] = Nil): Option[Long] = (line, bracketStack) match {
    case (Nil, stack) => Some(stack.foldLeft(0L)((score, b) => 5 * score + b.autocompleteScore))
    case (OpenBracket(b) :: rest, stack) => score2(rest, b :: stack)
    case (CloseBracket(b) :: rest, head :: stack) if head == b => score2(rest, stack)
    case _ => None // corrupted
  }

  def s2(input: String): Long = {
    val scores = input.linesIterator.flatMap(line => score2(line.toList)).toIndexedSeq.sorted
    scores(scores.length / 2)
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 26397
      theAnswerIs(s1(input), 216297)
    }
    test("star2") {
      s2(testInput) ==> 288957
      theAnswerIs(s2(input), 2165057169L)
    }
  }
}

