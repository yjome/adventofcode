package me.yjo.aoc.y2021

import me.yjo.util.*
import utest.*

object Day06 extends DayBase {

  override lazy val testInput = "3,4,3,1,2"

  def simStep(state: Seq[Long]): Seq[Long] = state match
    case Seq(x0, x1, x2, x3, x4, x5, x6, x7, x8) =>
      Seq(x1, x2, x3, x4, x5, x6, x7 + x0, x8, x0)

  def simulateFish(input: String, iterations: Int): Long = {
    val fishCounts = readInts(input).groupBy(identity)
    val initialState = (0 to 8).map(i => fishCounts.getOrElse(i, Set.empty).size.toLong)
    val endState = nTimes(iterations, simStep, initialState)
    endState.sum
  }

  def s1(input: String): Long = {
    simulateFish(input, 80)
  }

  def s2(input: String): Long = {
    simulateFish(input, 256)
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 5934
      theAnswerIs(s1(input), 386755)
    }
    test("star2") {
      s2(testInput) ==> 26984457539L
      theAnswerIs(s2(input), 1732731810807L)
    }
  }
}

