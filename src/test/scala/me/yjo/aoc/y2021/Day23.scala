package me.yjo.aoc.y2021

import me.yjo.aoc.y2021.Day23.Burrow.{destBurrow, plan}
import me.yjo.graph.{Area, Coord, Search}
import me.yjo.util.*
import utest.*

import scala.collection.immutable.MultiDict

object Day23 extends DayBase {
  val dest =
    """#############
      |#..~.~.~.~..#
      |###A#B#C#D###
      |  #A#B#C#D#
      |  #########""".stripMargin

  def podMoveCost(pod: Char) = pod match {
    case 'A' => 1
    case 'B' => 10
    case 'C' => 100
    case 'D' => 1000
  }

  def isHallway(c: Coord): Boolean = c.y == 1

  def range(x: Int, y: Int): Range = x.min(y) to x.max(y)

  case class Burrow(pods: MultiDict[Char, Coord]) {
    val occupied = pods.map(_._2).toSet
    require(pods.sets.forall(_._2.size == 2) && occupied.sizeIs == 8)

    def isDone = pods.forall({ case (pod, c) => plan.get(pod).contains(c) })
    def moves(pod: Char, c: Coord): Seq[(Coord, Int)] = {
      val toHallMoves =
        if c.y == 2 || c.y == 3 && !occupied.contains(c.copy(y = 2)) && !plan.get(pod).contains(c) then
          plan.get('.').view.filter(target => range(c.x, target.x).forall(x => !occupied.contains(Coord(x, 1)))).toList
        else Nil
      val toRoomMoves =
        if c.y == 3 || c.y == 2 && !plan.get(pod).contains(c) then Nil else
          plan.get(pod).view.filter(target =>
            !occupied.contains(target)
              && (target.y == 2 || target.x == c.x || !occupied.contains(target.copy(y = 2)))
              && range(target.x, c.x).forall(x => {
              val c1 = Coord(x, 1)
              c == c1 || !occupied.contains(c1)
            })
          ).toList
      (toHallMoves ++ toRoomMoves).map(target => (target, c.manhattanDist(target) * podMoveCost(pod))).toSeq
    }
    def moves: Seq[(Burrow, Int)] = (for {
      (pod, c) <- pods.view
      (target, cost) <- moves(pod, c)
    } yield (Burrow(pods - (pod -> c) + (pod -> target)), cost)).toSeq

    def minDistToDone: Int = {
      pods.collect({
        case (pod, c) if !plan.get(pod).contains(c) =>
          c.manhattanDist(plan.get(pod).head.copy(y = 2)) * podMoveCost(pod)
      }).sum
    }

    def solve = Search.aStar[Burrow, Int](this, destBurrow, _.moves, _.minDistToDone).get
  }

  object Burrow {
    val plan = MultiDict.from(Area.unRender(dest).map(_.swap))
    val destBurrow = parse(dest)
    def parse(input: String): Burrow = Burrow(MultiDict.from(Area.unRender(input).map(_.swap)).filterSets(_._1.isUpper))
  }

  def s1(input: String): Long = {
    Burrow.parse(input).solve.totalDist
  }

  def s2(input: String): Long = {
    ???
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 12521
      theAnswerIs(s1(input), 10321)
    }
    test("star2") {
      s2(testInput) ==> ???
      theAnswerIs(s2(input))
    }
  }
}

