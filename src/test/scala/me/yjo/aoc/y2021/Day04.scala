package me.yjo.aoc.y2021

import me.yjo.util.*
import utest.*

import scala.collection.View
import scala.collection.immutable.ArraySeq

object Day04 extends DayBase {

  class Board(input: String) {
    val rows: ArraySeq[ArraySeq[Int]] = ArraySeq.from(input.linesIterator.map(readInts))
    val cols = rows.transpose

    def allNums = rows.view.flatten

    def hasBingo(draws: Set[Int]): Boolean =
      rows.exists(_.forall(draws)) || cols.exists(_.forall(draws))
  }

  private def winningScores(input: String): View[Int] = {
    val Array(drawsStr, boardStrs*) = input.split("\n\n")
    val drawSequence = readInts(drawsStr)
    val boards = boardStrs.map(Board(_))

    val boardsNScores = for {
      i <- (5 to drawSequence.length).view
      draws = drawSequence.take(i).toSet
      board <- boards
      if board.hasBingo(draws)
      unmarked = (board.allNums.toSet -- draws).sum
      score = unmarked * drawSequence(i - 1)
    } yield (board, score)
    View.DistinctBy(boardsNScores, _._1).map(_._2)
  }

  def s1(input: String): Long = {
    winningScores(input).head
  }

  def s2(input: String): Long = {
    winningScores(input).last
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 4512
      theAnswerIs(s1(input), 27027)
    }
    test("star2") {
      s2(testInput) ==> 1924
      theAnswerIs(s2(input), 36975)
    }
  }
}

