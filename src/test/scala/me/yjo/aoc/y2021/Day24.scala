package me.yjo.aoc.y2021

import me.yjo.util.*
import utest.*


object Day24 extends DayBase {
  case class InputRound(i: Int, isPop: Boolean, testOffset: Int, pushOffset: Int)

  def parse(input: String): IndexedSeq[InputRound] = {
    val blocks = input.linesIterator.toIndexedSeq.grouped(18)
    blocks.zipWithIndex.map { case (lines, i) =>
      val s"add x ${IntString(testOffset)}" = lines(5)
      val s"add y ${IntString(pushOffset)}" = lines(15)
      InputRound(i, lines(4) == "div z 26", testOffset, pushOffset)
    }.toIndexedSeq
  }

  def s1(input: String): Long = {
    val rounds = parse(input)


    ???
  }

  def s2(input: String): Long = {
    ???
  }

  val tests = Tests {
    test("star1") {
      theAnswerIs(s1(input))
    }
    test("star2") {
      theAnswerIs(s2(input))
    }
  }
}

