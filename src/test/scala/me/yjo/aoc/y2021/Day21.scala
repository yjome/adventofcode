package me.yjo.aoc.y2021

import me.yjo.util.*
import utest.*

import java.lang.Math.floorMod
import scala.Function.untupled
import scala.annotation.tailrec

object Day21 extends DayBase {

  case class State1(p1: Long, p2: Long, p1Score: Long = 0, p2Score: Long = 0, rolls: Long = 0) {
    @tailrec
    final def play(): Long = {
      if p2Score >= 1000 then p1Score * rolls else
        val roll = rolls % 100 + (rolls + 1) % 100 + rolls + 2 % 100 + 3
        val newP1 = (p1 - 1 + roll) % 10 + 1
        State1(p2, newP1, p2Score, p1Score + newP1, rolls + 3).play()
    }
  }

  val rollsAndCounts = (for (r1 <- 1 to 3; r2 <- 1 to 3; r3 <- 1 to 3)
    yield r1 + r2 + r3).groupBy(identity).view.mapValues(_.size).toList

  case class State2(p1: Long, p2: Long, p1Score: Long, p2Score: Long)

  def s2(startP1: Int, startP2: Int): Long = {
    lazy val outcomesFrom: State2 => (Long, Long) = memoise {
      case State2(_, _, s1, _) if s1 >= 21 => (1, 0)
      case State2(_, _, _, s2) if s2 >= 21 => (0, 1)
      case State2(p1, p2, s1, s2) => (for (roll, count) <- rollsAndCounts yield {
        val newPos = floorMod(p1 + roll - 1, 10) + 1
        val (n1, n2) = outcomesFrom(State2(p2, newPos, s2, s1 + newPos))
        (n2 * count, n1 * count)
      }).reduce { case ((n1acc, n2acc), (n1, n2)) => (n1acc + n1, n2acc + n2) }
    }

    val (n1, n2) = outcomesFrom(State2(startP1, startP2, 0, 0))
    n1 max n2
  }

  val tests = Tests {
    test("star1") {
      State1(4, 8).play() ==> 739785
      theAnswerIs(State1(8, 3).play(), 412344)
    }
    test("star2") {
      s2(4, 8) ==> 444356092776315L
      theAnswerIs(s2(8, 3), 214924284932572L)
    }
  }
}

