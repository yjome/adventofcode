package me.yjo.aoc.y2021

import me.yjo.graph.{Area, Coord}
import me.yjo.util.*
import utest.*

object Day13 extends DayBase {

  enum Axis:
    case X, Y

  case class Fold(axis: Axis, line: Int) extends Function[Coord, Coord]:
    def apply(p: Coord) =
      if axis == Axis.X && p.x > line then Coord(line * 2 - p.x, p.y)
      else if axis == Axis.Y && p.y > line then Coord(p.x, line * 2 - p.y)
      else p

  def parseInput(input: String): (Set[Coord], List[Fold]) = {
    val Array(psStr, foldsStr) = input.split("\n\n")
    val points = psStr.linesIterator.map {
      case s"${IntString(x)},${IntString(y)}" => Coord(x, y)
    }.toSet
    val folds = foldsStr.linesIterator.map {
      case s"fold along x=${IntString(line)}" => Fold(Axis.X, line)
      case s"fold along y=${IntString(line)}" => Fold(Axis.Y, line)
    }.toList
    (points, folds)
  }

  def s1(input: String): Long = {
    val (points, fold :: _) = parseInput(input)
    points.map(fold).size
  }

  def s2(input: String): String = {
    val (points, folds) = parseInput(input)
    val result = folds.foldLeft(points)(_ map _)
    Area.rangeOf(result).render(p => if result contains p then '█' else '░')
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 17
      theAnswerIs(s1(input), 675)
    }
    test("star2") {
      theAnswerIs(s2(input),
        """█░░█░████░█░░█░█░░█░████░████░░░██░████
          |█░░█░░░░█░█░█░░█░░█░█░░░░█░░░░░░░█░░░░█
          |████░░░█░░██░░░████░███░░███░░░░░█░░░█░
          |█░░█░░█░░░█░█░░█░░█░█░░░░█░░░░░░░█░░█░░
          |█░░█░█░░░░█░█░░█░░█░█░░░░█░░░░█░░█░█░░░
          |█░░█░████░█░░█░█░░█░█░░░░████░░██░░████""".stripMargin)
    }
  }
}

