package me.yjo.aoc.y2021

import me.yjo.graph.{Area, Coord}
import me.yjo.util.*
import utest.*

import scala.collection.immutable.BitSet

object Day20 extends DayBase {

  case class Enhancement(litRules: BitSet) extends Function[Image, Image] {
    override def apply(in: Image): Image = {
      val newLit = in.area.grow().filter(c => litRules(in.neighbourhoodCode(c))).toSet
      Image(newLit, if in.isBgLit then litRules(511) else litRules(0))
    }
  }

  case class Image(lit: Set[Coord], isBgLit: Boolean = false) {
    val area = Area.rangeOf(lit)
    def apply(c: Coord): Boolean = if area contains c then lit(c) else isBgLit
    def neighbourhoodCode(at: Coord): Int = {
      val neighbourhood = for {
        y <- at.y - 1 to at.y + 1
        x <- at.x - 1 to at.x + 1
        n = Coord(x, y)
      } yield if apply(n) then 1 else 0
      neighbourhood.foldLeft(0)((acc, bit) => acc * 2 + bit)
    }
    override def toString = area.render(c => if lit(c) then '#' else '.')
  }

  def parse(input: String): (Enhancement, Image) = {
    val Array(enhancementStr, initStr) = input.split("\n\n")
    enhancementStr.length ==> 512
    val enhancement = Enhancement(BitSet.fromSpecific(enhancementStr.zipWithIndex.collect { case ('#', i) => i }))
    val initImage = Image(Area.unRender(initStr).collect({ case (c, '#') => c }).toSet)
    (enhancement, initImage)
  }

  def s1(input: String): Long = {
    val (enhancement, initImage) = parse(input)
    enhancement(enhancement(initImage)).lit.size
  }

  def s2(input: String): Long = {
    val (enhancement, initImage) = parse(input)
    nTimes(50, enhancement, initImage).lit.size
  }

  val tests = Tests {
    test("star1") {
      parse(testInput)._2.neighbourhoodCode(Coord(2, 2)) ==> 34

      s1(testInput) ==> 35
      val ans = s1(input)
      theAnswerIs(ans, 5291)
    }
    test("star2") {
      s2(testInput) ==> 3351
      theAnswerIs(s2(input), 16665)
    }
  }
}

