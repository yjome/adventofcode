package me.yjo.aoc.y2021

import me.yjo.util.*
import utest.*
import scodec.bits.*
import scodec.codecs.*
import me.yjo.aoc.y2021.bits.*

object Day16 extends DayBase {

  def s1(input: String): Long =
    Packet.decodeHex(input).flattened.map(_.version.toInt).sum

  def s2(input: String): Long =
    Packet.decodeHex(input).evaluate

  val tests = Tests {
    test("star1") {
      Packet.decodeHex("D2FE28") ==> Literal(6, 2021)
      Packet.decodeHex("38006F45291200") ==> Operator(1, 6, List(Literal(6, 10), Literal(2, 20)))
      s1("A0016C880162017C3686B18A3D4780") ==> 31
      theAnswerIs(s1(input), 965)
    }
    test("star2") {
      s2("9C0141080250320F1802104A08") ==> 1
      theAnswerIs(s2(input), 116672213160L)
    }
  }
}

