package me.yjo.aoc.y2021

import me.yjo.graph.Coord.origin
import me.yjo.graph.{Area, Coord}
import me.yjo.util.*
import utest.*

import scala.annotation.tailrec
import scala.math.{min, signum}

object Day17 extends DayBase {

  val testTarget = Area(Coord(20, -10), Coord(30, -5))
  val target = Area(Coord(217, -126), Coord(240, -69))

  case class Probe(pos: Coord, vel: Coord) {
    def step: Probe =
      Probe(pos + vel, Coord(vel.x - signum(vel.x), vel.y - 1))

    @tailrec
    final def willReach(target: Area): Boolean =
      target.contains(pos) ||
        (pos.x <= target.bottomRight.x
          && (pos.y >= target.topLeft.y || vel.y > 0)
          && step.willReach(target))

    @tailrec
    final def maxY: Long = if vel.y <= 0 then pos.y else step.maxY
  }

  def onTargetLaunches(target: Area) = {
    for {
      dx <- 6 to 240
      dy <- -126 to 500
      launch = Probe(origin, Coord(dx, dy))
      if launch.willReach(target)
    } yield launch
  }

  def s1(target: Area): Long = onTargetLaunches(target).map(_.maxY).max

  def s2(target: Area): Long = onTargetLaunches(target).size

  val tests = Tests {
    test("star1") {
      s1(testTarget) ==> 45
      theAnswerIs(s1(target), 7875)
    }
    test("star2") {
      s2(testTarget) ==> 112
      theAnswerIs(s2(target), 2321)
    }
  }
}

