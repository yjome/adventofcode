package me.yjo.aoc.y2021

import me.yjo.graph.Coord
import me.yjo.util.*
import utest.*

import scala.math.{abs, signum}

object Day05 extends DayBase {

  case class Line(x1: Int, y1: Int, x2: Int, y2: Int) {
    val isOrthogonal = x1 == x2 || y1 == y2

    def points: Iterator[Coord] = {
      val (dx, dy) = (signum(x2 - x1), signum(y2 - y1))
      val iMax = scala.math.max(abs(x2 - x1), abs(y2 - y1))
      for (i <- (0 to iMax).iterator)
        yield Coord(x1 + dx * i, y1 + dy * i)
    }
  }

  private def readLines(input: String): Seq[Line] =
    input.linesIterator.map {
      case s"${IntString(x1)},${IntString(y1)} -> ${IntString(x2)},${IntString(y2)}" =>
        Line(x1, y1, x2, y2)
    }.toSeq

  def s1(input: String): Long = {
    readLines(input).filter(_.isOrthogonal)
      .flatMap(_.points).groupBy(identity)
      .count(_._2.length >= 2)
  }

  def s2(input: String): Long = {
    readLines(input)
      .flatMap(_.points).groupBy(identity)
      .count(_._2.length >= 2)
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 5
      theAnswerIs(s1(input), 5608)
    }
    test("star2") {
      s2(testInput) ==> 12
      theAnswerIs(s2(input), 20299)
    }
  }
}

