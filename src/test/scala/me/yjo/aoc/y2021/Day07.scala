package me.yjo.aoc.y2021

import me.yjo.util.*
import utest.*

import scala.math.abs

object Day07 extends DayBase {

  override lazy val testInput = "16,1,2,0,4,2,7,1,2,14"

  def s1(input: String): Long = {
    val xs = readInts(input)
    val range = xs.min to xs.max
    range.map(i => xs.map(x => abs(x - i).toLong).sum).min
  }

  def s2(input: String): Long = {
    val xs = readInts(input)
    val range = xs.min to xs.max
    range.map(i => xs.map(x => {
      val dist = abs(x - i).toLong
      dist * (dist + 1) / 2
    }).sum).min
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 37
      theAnswerIs(s1(input), 335271)
    }
    test("star2") {
      s2(testInput) ==> 168
      theAnswerIs(s2(input), 95851339)
    }
  }
}

