package me.yjo.aoc.y2021

import me.yjo.util.*
import utest.*

object Day12 extends DayBase {

  override lazy val testInput = "start-A\nstart-b\nA-c\nA-b\nb-d\nA-end\nb-end"
  val testInput2 = "dc-end\nHN-start\nstart-kj\ndc-start\ndc-HN\nLN-dc\nHN-end\nkj-sa\nkj-HN\nkj-dc"
  val testInput3 = "fs-end\nhe-DX\nfs-he\nstart-DX\npj-DX\nend-zg\nzg-sl\nzg-pj\npj-he\nRW-he\nfs-DX\npj-RW\nzg-RW\nstart-pj\nhe-WI\nzg-he\npj-fs\nstart-RW"

  case class Cave(name: String) {
    def isSmall = name(0).isLower
  }

  def parseMap(input: String): Map[Cave, Set[Cave]] = {
    input.linesIterator.flatMap {
      case s"${cave1}-${cave2}" => Iterator(Cave(cave1) -> Cave(cave2), Cave(cave2) -> Cave(cave1))
    }.toSeq.groupBy(_._1).view.mapValues(_.map(_._2).toSet).toMap
  }

  def paths(map: Map[Cave, Set[Cave]], at: Cave = Cave("start"), smallsVisited: Set[Cave] = Set.empty): Int = {
    if at == Cave("end") then 1 else
      map(at).diff(smallsVisited).view.map(n => paths(map, n, if at.isSmall then smallsVisited + at else smallsVisited)).sum
  }

  def s1(input: String): Long = {
    paths(parseMap(input))
  }

  def paths2(map: Map[Cave, Set[Cave]], at: Cave = Cave("start"), smallsVisited: Set[Cave] = Set.empty, hasRevisited: Boolean = false): Int = {
    if at == Cave("end") then 1 else {
      val newVisited = if at.isSmall then smallsVisited + at else smallsVisited
      map(at).view.filter(n =>
        n != Cave("start") && (!n.isSmall || !hasRevisited || !smallsVisited.contains(n))
      ).map(n => paths2(map, n, newVisited, hasRevisited || smallsVisited.contains(n))).sum
    }
  }

  def s2(input: String): Long = {
    paths2(parseMap(input))
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 10
      s1(testInput2) ==> 19
      s1(testInput3) ==> 226
      theAnswerIs(s1(input), 3856)
    }
    test("star2") {
      s2(testInput) ==> 36
      s2(testInput2) ==> 103
      s2(testInput3) ==> 3509
      theAnswerIs(s2(input), 116692)
    }
  }
}

