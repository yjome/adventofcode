package me.yjo.aoc.y2021

import me.yjo.util.*
import utest.*

object Day08 extends DayBase {
  val digits = Map(
    "abcefg" -> 0,
    "cf" -> 1,
    "acdeg" -> 2,
    "acdfg" -> 3,
    "bcdf" -> 4,
    "abdfg" -> 5,
    "abdefg" -> 6,
    "acf" -> 7,
    "abcdefg" -> 8,
    "abcdfg" -> 9,
  )

  def unscramble(permutation: String, wires: String) = wires.map(c => permutation(c - 'a')).sorted

  case class InputLine(scrambledDigits: Seq[String], display: Seq[String]) {
    def decode(): Int = {
      val permutation = "abcdefg".permutations.find { perm =>
        scrambledDigits.forall(scrambled => digits.contains(unscramble(perm, scrambled)))
      }.get
      val Seq(d0, d1, d2, d3) = display.map(str => digits(unscramble(permutation, str)))
      d0 * 1000 + d1 * 100 + d2 * 10 + d3
    }
  }

  object InputLine {
    def apply(line: String): InputLine = {
      val s"${part1} | ${part2}" = line
      val scrambledDigits = part1.split(' ').sortBy(_.length)
      val display = part2.split(' ')
      InputLine(scrambledDigits, display)
    }
  }

  def s1(input: String): Long = {
    input.linesIterator.map(InputLine(_)).flatMap(_.display).map(_.length)
      .count(Seq(2, 4, 3, 7) contains _)
  }

  def s2(input: String): Long = {
    input.linesIterator.map(InputLine(_).decode()).sum
  }

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 26
      theAnswerIs(s1(input), 261)
    }
    test("star2") {
      s2(testInput) ==> 61229
      theAnswerIs(s2(input), 987553)
    }
  }
}

