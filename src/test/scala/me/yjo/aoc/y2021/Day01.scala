package me.yjo.aoc.y2021

import me.yjo.util.*
import utest.*

import scala.collection.IterableOps
import scala.collection.immutable.ArraySeq

object Day01 extends DayBase {

  override lazy val testInput = "199\n200\n208\n210\n200\n207\n240\n269\n260\n263"

  def s1(input: String): Long =
    val depths = readInts(input)
    pairs(depths).count(_ < _)

  def s2(input: String): Long =
    val depths = readInts(input)
    val windowDepths = depths.sliding(3).map(_.sum).toSeq
    pairs(windowDepths).count(_ < _)

  val tests = Tests {
    test("star1") {
      s1(testInput) ==> 7
      theAnswerIs(s1(input), 1832)
    }
    test("star2") {
      s2(testInput) ==> 5
      theAnswerIs(s2(input), 1858)
    }

    test("Exact int implicits resolved") {
      Seq(Int.MaxValue, 0).sum
      Seq(Long.MaxValue, 0).sum
      intercept[ArithmeticException](Seq(Int.MaxValue, 1).sum)
      intercept[ArithmeticException](Seq(Long.MaxValue, 1).sum)
    }
  }
}
