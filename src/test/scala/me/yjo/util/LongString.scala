package me.yjo.util

object LongString {
  def unapply(v: String): Option[Long] = v.toLongOption
}
