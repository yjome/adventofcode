package me.yjo.util

object IntString {
  def unapply(v: String): Option[Int] = v.toIntOption
}
