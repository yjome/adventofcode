package me.yjo

import scala.annotation.tailrec
import scala.collection.immutable.ArraySeq
import scala.collection.{MapView, mutable}

package object util {
  @tailrec
  def nTimes[A](n: Int, f: A => A, a: A): A =
    if (n == 0) a else nTimes(n - 1, f, f(a))

  def counts[T](xs: IterableOnce[T]): MapView[T, Long] = {
    val acc = mutable.HashMap[T, Long]()
    xs.iterator.foreach(acc.updateWith(_)(_.map(_ + 1L).orElse(Some(1L))))
    acc.view
  }

  def pairs[T](seq: Seq[T]): Seq[(T, T)] =
    seq.zip(seq.tail)

  def readInts(input: String) = ArraySeq.from(input.trim.split("[,\\n\\s]+").map(_.trim.toInt))

  def readLongs(input: String) = ArraySeq.from(input.trim.split("[,\\n\\s]+").map(_.trim.toLong))

  def memoise[In, Out](f: In => Out): In => Out = {
    val map = new mutable.HashMap[In, Out]
    (in: In) => map.getOrElseUpdate(in, f(in))
  }

  implicit class RegexOps(sc: StringContext) {
    def r = sc.parts.mkString.r
  }
}
