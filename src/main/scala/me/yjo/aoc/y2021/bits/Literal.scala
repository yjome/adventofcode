package me.yjo.aoc.y2021.bits

import scodec.Attempt.{Failure, Successful}
import scodec.bits.*
import scodec.codecs.*
import scodec.{bits as _, *}

case class Literal(version: Byte, value: Long) extends Packet {
  override type P = Literal
  override def companion: Literal.type = Literal
  override def pType: Byte = Literal.pType
  override def evaluate: Long = value
}

object Literal extends PacketCompanion[Literal] {
  val pType: Byte = 4
  val value: Codec[Long] = new Decoder[Long] {
    override def decode(input: BitVector): Attempt[DecodeResult[Long]] = {
      var rest = input
      var buffer = BitVector.empty
      while {
        if rest.sizeLessThan(5) then
          return Failure(new Err.InsufficientBits(5, rest.size))

        val hasMore = rest.head
        val nibble = rest.slice(1, 5)
        rest = rest.drop(5)
        buffer ++= nibble
        hasMore
      } do ()
      Successful(DecodeResult(buffer.toLong(signed = false), rest))
    }
  }.decodeOnly

  override val codec: Codec[Literal] = {
    ("version" | ubyte(3)) ::
      ("type" | constant(BitVector.fromInt(pType, 3))) ::
      ("value" | value)
  }.dropUnits.as[Literal]
}
