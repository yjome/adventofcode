package me.yjo.aoc.y2021.bits

import scodec.Attempt.{Failure, Successful}
import scodec.bits.*
import scodec.codecs.*
import scodec.{bits as _, *}

import scala.collection.View

case class Operator(version: Byte, pType: Byte, operands: List[Packet]) extends Packet {
  override type P = Operator
  override def companion: Operator.type = Operator
  override def flattened: View[Packet] = super.flattened ++ operands.view.flatMap(_.flattened)
  //noinspection ZeroIndexToHead
  override def evaluate: Long = {
    val args = operands.map(_.evaluate)
    pType match {
      case 0 => args.sum
      case 1 => args.product
      case 2 => args.min
      case 3 => args.max
      case 5 => if args(0) > args(1) then 1 else 0
      case 6 => if args(0) < args(1) then 1 else 0
      case 7 => if args(0) == args(1) then 1 else 0
    }
  }
}

object Operator extends PacketCompanion[Operator] {
  override val codec: Codec[Operator] = {
    val bitLengthOperands: Codec[List[Packet]] = variableSizeBits(uint(15), list(Packet.codec))
    val countedOperands: Codec[List[Packet]] = listOfN(uint(11), Packet.codec)
    val operands: Codec[List[Packet]] = either(bool, bitLengthOperands, countedOperands).xmap(_.merge, Right(_))

    (("version" | ubyte(3)) ::
      ("type" | ubyte(3)) ::
      ("operands" | operands)
      ).dropUnits.as[Operator]
  }
}
