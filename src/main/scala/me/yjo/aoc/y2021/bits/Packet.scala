package me.yjo.aoc.y2021.bits

import me.yjo.aoc.y2021.bits.Literal
import me.yjo.aoc.y2021.bits.Literal.pType
import scodec.Attempt.{Failure, Successful}
import scodec.bits.*
import scodec.codecs.*
import scodec.{bits as _, *}

import scala.collection.View

trait Packet { self =>
  protected[bits] type P >: self.type <: Packet
  protected[bits] def companion: PacketCompanion[P]
  protected[bits] def instance: P = self
  def version: Byte
  def pType: Byte

  def flattened: View[Packet] = View(this)
  def evaluate: Long
}

object Packet {
  def decodeHex(hex: String): Packet =
    codec.decode(BitVector.fromValidHex(hex)).require.value

  given codec: Codec[Packet] = Codec(
    packet => packet.companion.codec.encode(packet),
    bits => {
      bits.slice(3, 6).toInt(false) match {
        case Literal.pType => Literal.codec.decode(bits)
        case _ => Operator.codec.decode(bits)
      }
    })
}

trait PacketCompanion[P <: Packet] {
  given codec: Codec[P]
}
