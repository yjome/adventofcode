package me.yjo.graph

import me.yjo.stringUtils
import me.yjo.stringUtils.Stringy

import scala.Function.const

// +ve x is to the right, +ve y is downwards
case class Area(topLeft: Coord, bottomRight: Coord) extends Iterable[Coord] {
  require(bottomRight.x >= topLeft.x && topLeft.y <= bottomRight.y)

  def width: Int = bottomRight.x - topLeft.x + 1

  def height: Int = bottomRight.y - topLeft.y + 1

  def isSquare: Boolean = width == height

  def isLine: Boolean = width == 1 || height == 1

  def transposed: Area = Area(topLeft.transposed, bottomRight.transposed)

  def render[T <: Stringy](getChar: Coord => T): String = {
    (for (y <- topLeft.y to bottomRight.y)
      yield (for (x <- topLeft.x to bottomRight.x)
        yield getChar(Coord(x, y))).mkString).mkString("\n")
  }

  def contains(pt: Coord): Boolean =
    pt.x >= topLeft.x && pt.x <= bottomRight.x &&
      pt.y >= topLeft.y && pt.y <= bottomRight.y

  def perimeterContains(pt: Coord): Boolean = contains(pt) &&
    (pt.x == topLeft.x || pt.x == bottomRight.x ||
      pt.y == topLeft.y || pt.y == bottomRight.y)

  def shrink(x: Int = 1): Area = Area(topLeft + Coord(x, x), bottomRight - Coord(x, x))

  def grow(x: Int = 1): Area = Area(topLeft - Coord(x, x), bottomRight + Coord(x, x))

  def unionBounds(other: Area): Area = Area(
    Coord(Math.min(topLeft.x, other.topLeft.x), Math.min(topLeft.y, other.topLeft.y)),
    Coord(Math.max(bottomRight.x, other.bottomRight.x), Math.max(bottomRight.y, other.bottomRight.y))
  )

  override def iterator: Iterator[Coord] = for {
    y <- (topLeft.y to bottomRight.y).iterator
    x <- (topLeft.x to bottomRight.x).iterator
  } yield Coord(x, y)

  def side(d: CardinalDir) = d match {
    case CardinalDir.N => for (x <- (topLeft.x to bottomRight.x).view) yield Coord(x, topLeft.y)
    case CardinalDir.E => for (y <- (topLeft.y to bottomRight.y).view) yield Coord(bottomRight.x, y)
    case CardinalDir.S => for (x <- (topLeft.x to bottomRight.x).view) yield Coord(x, bottomRight.y)
    case CardinalDir.W => for (y <- (topLeft.y to bottomRight.y).view) yield Coord(topLeft.x, y)
  }
  override def toString(): String = s"Area($topLeft, $bottomRight)"
}

object Area {
  def apply(width: Int, height: Int): Area = Area(Coord.origin, Coord(width - 1, height - 1))

  def ofPoint(point: Coord): Area = Area(point, point)

  def unRender(string: String): Seq[(Coord, Char)] = for {
    (line, y) <- string.linesIterator.zipWithIndex.toSeq
    (c, x) <- line.zipWithIndex
  } yield (Coord(x, y), c)

  def rangeOf(coords: Iterable[Coord]): Area =
    coords.map(Area.ofPoint).reduce(_ unionBounds _)

  def rangeOf(str: String): Area =
    Area(Coord.origin, Coord(str.linesIterator.map(_.length).max - 1, str.linesIterator.size - 1))

}
