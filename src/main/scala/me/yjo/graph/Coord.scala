package me.yjo.graph

case class Coord(x: Int, y: Int) {

  def transposed = Coord(y, x)

  def step(dir: CardinalDir, steps: Int = 1) = Coord(x + dir.dx * steps, y + dir.dy * steps)

  def manhattanDist(p: Coord): Int = math.abs(p.x - x) + math.abs(p.y - y)

  def neighbours4Way: List[Coord] = CardinalDir.all.map(step(_))

  def neighbours8Way: IndexedSeq[Coord] = for {
    nx <- x - 1 to x + 1
    ny <- y - 1 to y + 1
    if nx != x || ny != y
  } yield Coord(nx, ny)

  def rotateLeftDeg(deg: Int) : Coord = {
    require(deg % 90 == 0)
    Math.floorMod(deg / 90, 4) match {
      case 0 => this
      case 1 => Coord(y, -x)
      case 2 => Coord(-x, -y)
      case 3 => Coord(-y, x)
    }
  }

  def rotateRightDeg(deg: Int) : Coord = rotateLeftDeg(-deg)

  def +(that: Coord) = Coord(this.x + that.x, this.y + that.y)

  def -(that: Coord) = Coord(this.x - that.x, this.y - that.y)

  def *(factor: Int) = Coord(x * factor, y * factor)
}

object Coord {
  val origin: Coord = Coord(0, 0)
}
