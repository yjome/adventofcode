package me.yjo.graph

import me.yjo.graph.Search.RouteResult
import me.yjo.stringUtils
import me.yjo.stringUtils.Stringy
import scodec.Codec

import scala.Function.const
import scala.Specializable.Bits32AndUp
import scala.annotation.tailrec
import scala.collection.immutable.{IndexedSeq, Queue}
import scala.collection.mutable.ArrayBuffer
import scala.collection.{IndexedSeqView, View, immutable, mutable}
import scala.math
import scala.math.Ordering.Double
import scala.reflect.ClassTag

object Search {

  case class RouteResult[TNode, TDist: Numeric](steps: IndexedSeq[(TNode, /*cumulative*/ TDist)]) {
    require(steps.nonEmpty)
    def length: Int = steps.size // Includes both ends of route
    def totalDist: TDist = steps.last._2
    def visitedNodes: IndexedSeqView[TNode] = steps.view.map(_._1)

    def render(area: Area, field: Coord => Stringy = const('░'), path: Coord => Stringy = const('*'))
      (using asCoord: TNode => Coord): String = {
      val visitedSet = visitedNodes.map(asCoord).toSet
      area.render(c => if visitedSet contains c then path(c) else field(c))
    }

    def renderBold(area: Area, field: Coord => Stringy)(using asCoord: TNode => Coord): String =
      render(area, field, field.andThen(stringUtils.bold))
  }

  def aStar[TNode, @specialized(Bits32AndUp) TDist: Numeric](
    from: TNode, to: TNode,
    neighboursAndHopDists: TNode => Iterable[(TNode, TDist)],
    optimisticDistToEnd: TNode => TDist
  ): Option[RouteResult[TNode, TDist]] = {
    Search(from, to, neighboursAndHopDists, optimisticDistToEnd).run()
  }

  def dijkstra[TNode, @specialized(Bits32AndUp) TDist: Numeric](
    from: TNode, to: TNode,
    neighboursAndHopDists: TNode => Iterable[(TNode, TDist)]
  ): Option[RouteResult[TNode, TDist]] = {
    val trivialHeuristic = const(summon[Numeric[TDist]].zero)
    Search(from, to, neighboursAndHopDists, trivialHeuristic).run()
  }
}

private class Search[TNode, @specialized(Bits32AndUp) TDist: Numeric](
  val start: TNode, val dest: TNode,
  val neighboursAndHopDists: TNode => Iterable[(TNode, TDist)],
  val optimisticDistToEnd: TNode => TDist
) {

  import Numeric.Implicits.*
  import math.Ordering.Implicits.infixOrderingOps

  private val zeroDist = summon[Numeric[TDist]].zero

  final def estimateDistanceVia(route: Route): TDist =
    route.totalDist + optimisticDistToEnd(route.to)

  given Ordering[Route] = Ordering.by(estimateDistanceVia).reverse

  private val closedNodes = mutable.Set[TNode]()
  private val bestRoutesSoFar = mutable.Map(start -> Route(start))
  private val toExpand = mutable.PriorityQueue(bestRoutesSoFar(start))

  def run(): Option[RouteResult[TNode, TDist]] = {
    while (toExpand.nonEmpty) {
      val r = toExpand.dequeue()
      if (r.to == dest)
        return Some(r.toRouteResult)

      if (closedNodes.add(r.to))
        neighboursAndHopDists(r.to).foreach { case (neighbour, hopDist) =>
          val totalDist = r.totalDist + hopDist
          if (beatsKnownRoutes(neighbour, totalDist)) {
            val newRoute = Route(neighbour, Some(r), totalDist)
            bestRoutesSoFar.put(neighbour, newRoute)
            toExpand.enqueue(newRoute)
          }
        }
    }
    None
  }

  case class Route(to: TNode, via: Option[Route] = None, totalDist: TDist = zeroDist) {
    val length: Int = via.map(_.length + 1).getOrElse(1)

    @tailrec
    final def start: TNode = via match {
      case Some(prev) => prev.start
      case None => to
    }

    private def ancestors: View[Route] = View.unfold(Option(this))(_.map(r => (r, r.via)))

    def toRouteResult: RouteResult[TNode, TDist] = {
      ancestors.map(r => (r.to, r.totalDist))
      val buffer = new Array[(TNode, TDist)](length)
      var i = length - 1
      var nextNode = Option(this)
      while (i >= 0) {
        val node = nextNode.get
        buffer(i) = (node.to, node.totalDist)
        i -= 1
        nextNode = node.via
      }
      RouteResult(immutable.ArraySeq.unsafeWrapArray(buffer))
    }
  }

  private def beatsKnownRoutes(neighbour: TNode, totalDist: TDist): Boolean =
    !closedNodes.contains(neighbour) && !bestRoutesSoFar.get(neighbour).exists(_.totalDist <= totalDist)
}
