package me.yjo.graph

import CardinalDir.{E, N, S, W}

sealed abstract class CardinalDir {
  def dx: Int

  def dy: Int

  def left: CardinalDir

  def right: CardinalDir

  def reverse: CardinalDir = left.left

  def leftDeg(deg: Int): CardinalDir = {
    require(deg % 90 == 0)
    Math.floorMod(deg / 90, 4) match {
      case 0 => this
      case 1 => left
      case 2 => reverse
      case 3 => right
    }
  }

  def rightDeg(deg: Int): CardinalDir = leftDeg(-deg)

  def toChar: Char = this match {
    case N => 'N'
    case E => 'E'
    case S => 'S'
    case W => 'W'
  }
}

object CardinalDir {

  val all: List[CardinalDir] = List(N, E, S, W)

  def apply(c: Char): CardinalDir = c match {
    case 'N' | 'U' => N
    case 'E' | 'R' => E
    case 'S' | 'D' => S
    case 'W' | 'L' => W
  }

  case object N extends CardinalDir {
    def dx: Int = 0
    def dy: Int = -1
    def left: CardinalDir = W
    def right: CardinalDir = E
  }

  case object E extends CardinalDir {
    def dx: Int = 1
    def dy: Int = 0
    def left: CardinalDir = N
    def right: CardinalDir = S
  }

  case object S extends CardinalDir {
    def dx: Int = 0
    def dy: Int = 1
    def left: CardinalDir = E
    def right: CardinalDir = W
  }

  case object W extends CardinalDir {
    def dx: Int = -1
    def dy: Int = 0
    def left: CardinalDir = S
    def right: CardinalDir = N
  }

}
