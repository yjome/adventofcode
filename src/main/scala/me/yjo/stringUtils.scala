package me.yjo

object stringUtils {
  import scala.io.AnsiColor as esc

  type Stringy = Char | CharSequence

  def bold(in: Stringy): String = {
    esc.BOLD + in + esc.RESET
  }

}
