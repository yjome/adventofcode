package me.yjo.maths

import scala.math.Numeric.LongIsIntegral
import scala.math.{Integral, Ordering}

object ExactMathsImplicits {

  trait IntIsExactIntegral extends Integral[Int] {
    def plus(x: Int, y: Int): Int = Math.addExact(x, y)
    def minus(x: Int, y: Int): Int = Math.subtractExact(x, y)
    def times(x: Int, y: Int): Int = Math.multiplyExact(x, y)
    def quot(x: Int, y: Int): Int = x / y
    def rem(x: Int, y: Int): Int = x % y
    def negate(x: Int): Int = Math.negateExact(x)
    def fromInt(x: Int): Int = x
    def parseString(str: String): Option[Int] = str.toIntOption
    def toInt(x: Int): Int = x
    def toLong(x: Int): Long = x.toLong
    def toFloat(x: Int): Float = x.toFloat
    def toDouble(x: Int): Double = x.toDouble
    override def signum(x: Int): Int = math.signum(x)
    override def sign(x: Int): Int = math.signum(x)
  }

  given IntIsExactIntegral: IntIsExactIntegral with Ordering.IntOrdering

  trait LongIsExactIntegral extends Integral[Long] {
    def plus(x: Long, y: Long): Long = Math.addExact(x, y)
    def minus(x: Long, y: Long): Long = Math.subtractExact(x, y)
    def times(x: Long, y: Long): Long = Math.multiplyExact(x, y)
    def quot(x: Long, y: Long): Long = x / y
    def rem(x: Long, y: Long): Long = x % y
    def negate(x: Long): Long = Math.negateExact(x)
    def fromInt(x: Int): Long = x.toLong
    def parseString(str: String): Option[Long] = str.toLongOption
    def toInt(x: Long): Int = Math.toIntExact(x)
    def toLong(x: Long): Long = x
    def toFloat(x: Long): Float = x.toFloat
    def toDouble(x: Long): Double = x.toDouble
    override def signum(x: Long): Int = math.signum(x).toInt
    override def sign(x: Long): Long = math.signum(x)
  }

  given LongIsExactIntegral: LongIsExactIntegral with Ordering.LongOrdering
}
